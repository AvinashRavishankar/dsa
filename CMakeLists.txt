cmake_minimum_required(VERSION 3.10.2)

project("Data Structures And Algorithms"
   VERSION 0.1
   DESCRIPTION "A header only data structure and algorithms project"
   LANGUAGES CXX)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

configure_file(headers/DSAConfig.h.in headers/DSAConfig.h)

add_library(DSA INTERFACE)

target_include_directories(DSA INTERFACE
   $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/headers>
   $<INSTALL_INTERFACE:include/dsa>)

install(DIRECTORY headers/
   DESTINATION include/dsa
   CONFIGURATIONS Release
   FILES_MATCHING PATTERN "*.hpp")

install(TARGETS DSA
   CONFIGURATIONS Release
   EXPORT dsa
   )

install(EXPORT dsa
   NAMESPACE dsa::
   DESTINATION /usr/lib/cmake/dsa)

option(BUILD_TESTS "builds tests if enabled" OFF)

if(BUILD_TESTS)
   message(STATUS "BUILD_TESTS enabled")
   add_subdirectory(tests)
else()
   message(STATUS "BUILD_TESTS disabled")
endif()

add_custom_target(compile_commands ALL
   COMMAND cp ${CMAKE_BINARY_DIR}/compile_commands.json compile_commands.json
   WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

# add_dependencies(compile_commands test_DSA)
