/*
 * Copyright 2021 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file SearchAlgorithms.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated sequential search algorithms.
 */

#ifndef SEARCHALGORITHMS_INCLUDED
#define SEARCHALGORITHMS_INCLUDED

namespace dsa
{

   /**
    * @brief A class for sequential search algorithm.
    */
   class SequentialSearch
   {
      public:

         /**
          * @brief () operator overload
          *
          * @tparam ObjType Type/Class of the object to operate on.
          * @tparam DataType Type of data in the object.
          * @param aStruct Reference to data structure.
          * @param record Value to be searched in the data structure.
          * @return Index of the record searched. If found, returns a
          * positive integer. Else, returns -1.
          */
         template<typename ObjType, typename DataType>
            int operator()(ObjType& aStruct, const DataType& record)
            {
               using Iterator = typename ObjType::Iterator;
               int index{-1};
               int tmpIndex{0};
               Iterator iter{aStruct.begin()};
               Iterator endIter{aStruct.end()};

               while(iter != endIter)
               {
                  if(*iter == record)
                  {
                     index = tmpIndex;
                     break;
                  }
                  ++iter;
                  ++tmpIndex;
               }

               return index;
            }
   };

   /**
    * @brief A class for binary search algorithm.
    */
   class BinarySearch
   {
      public:

         /**
          * @brief () operator overload
          *
          * @tparam ObjType Type/Class of the object to operate on.
          * @tparam DataType Type of data in the object.
          * @param aStruct Reference to data structure.
          * @param record Value to be searched in the data structure.
          * @return Index of the record searched. If found, returns a
          * positive integer. Else, returns -1.
          */
         template<typename ObjType, typename DataType>
            int operator()(ObjType& aStruct, const DataType& record)
            {
               using Iterator = typename ObjType::Iterator;

               int index{-1};
               int lIndex{0};
               int hIndex{aStruct.size() - 1};

               while(lIndex <= hIndex)
               {
                  int mIndex{(hIndex + lIndex) / 2};
                  Iterator iter{aStruct.begin() + mIndex};

                  if(*iter < record)
                  {
                     lIndex = mIndex + 1;
                  }
                  else if(*iter > record)
                  {
                     hIndex = mIndex - 1;
                  }
                  else
                  {
                     index = mIndex;
                     break;
                  }
               }
               return index;
            }
   };

   /**
    * @brief a class for Fibonacci search algorithm.
    */
   class FibonacciSearch
   {
      public:
         /** Look Up table to hold the Fibonacci sequence up to maximum
          * permissible value in 32-bit int data type.*/
         static constexpr int fibLUT[47] =
         {         0,          1,          1,          2,          3,
                   5,          8,         13,         21,         34,
                  55,         89,        144,        233,        377,
                 610,        987,       1597,       2584,       4181,
                6765,      10946,      17711,      28657,      46368,
               75025,     121393,     196418,     317811,     514229,
              832040,    1346269,    2178309,    3524578,    5702887,
             9227465,   14930352,   24157817,   39088169,   63245986,
           102334155,  165580141,  267914296,  433494437,  701408733,
          1134903170, 1836311903};

         /**
          * @brief () operator overload
          *
          * @tparam ObjType Type/Class of the object to operate on.
          * @tparam DataType Type of data in the object.
          * @param aStruct Reference to data structure.
          * @param record Value to be searched in the data structure.
          * @return Index of the record searched. If found, returns a
          * positive integer. Else, returns -1.
          */
         template<typename ObjType, typename DataType>
            int operator()(ObjType& aStruct, const DataType& record)
            {
               using Iterator = typename ObjType::Iterator;
               int index{-1};
               int fibIndex{0};
               int objSize{aStruct.size()};

               // Compute the index for the Fibonacci series, such that, the
               // Fibonacci value at the obtained index should be the largest
               // possible value in the range 0 to <objSize>.
               for(int i = 0; i < 47; ++i)
               {
                  if(fibLUT[i] >= objSize)
                  {
                     fibIndex = --i;
                     break;
                  }
               }

               int objIndex{fibLUT[fibIndex] - 1};

               // If the record index is greater than <objIndex>, adjust
               // <objIndex> such that the search size on the RHS is
               // <fibLUT[fibIndex - 1]>
               if(aStruct[objIndex] < record)
               {
                  objIndex = (objSize - 1) - fibLUT[fibIndex - 1];
               }

               while(((fibIndex - 2) >= 0) && (objIndex < objSize))
               {
                  Iterator iter{aStruct.begin() + objIndex};
                  if(*iter < record)
                  {
                     objIndex += fibLUT[fibIndex - 2];
                     --fibIndex;
                  }
                  else if(*iter > record)
                  {
                     objIndex -= fibLUT[fibIndex - 2];
                     --fibIndex;
                  }
                  else
                  {
                     index = objIndex;
                     break;
                  }
               }

               return index;
            }
   };

}

#endif
