/*
 * Copyright 2020 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file BinaryTreeNode.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated node class specialized for binary trees.
 */

#ifndef BINARYTREENODE_INCLUDED
#define BINARYTREENODE_INCLUDED

#include "DoubleNode.hpp"
#include <utility>

namespace dsa
{
#ifdef BINARYTREE_INCLUDED
   template<typename type> class BinaryTree;
#endif

#ifdef BINARYSEARCHTREE_INCLUDED
   template<typename type> class BinarySearchTree;
#endif

   /**
    * @brief A template node class for Binary Tree data structure.
    *
    * @tparam type Data type to store in the node.
    */
   template<typename type>
      class BinaryTreeNode: public DoubleNode<type>
      {
         private:

            using DNode_t  = DoubleNode<type>;
            using BTNode_t = BinaryTreeNode<type>;

#ifdef BINARYTREE_INCLUDED
            friend BinaryTree<type>;
#endif

#ifdef BINARYSEARCHTREE_INCLUDED
            friend BinarySearchTree<type>;
#endif

         protected:
            /**
             * @brief Fetches the left child node.
             *
             * @return Address of the left child node.
             */
            inline BTNode_t* _leftChild()
            {
               return static_cast<BTNode_t*>(DNode_t::_leftNode());
            }

            /**
             * @brief Fetches the right child node.
             *
             * @return Address of the right child node.
             */
            inline BTNode_t* _rightChild()
            {
               return static_cast<BTNode_t*>(DNode_t::_rightNode());
            }

            /**
             * @brief Sets the given node as left child.
             *
             * @param aLink Address of the Node to be linked to the left.
             */
            inline void _setLeftChild(BTNode_t* aLink)
            {
               DNode_t::_setLeftNode(aLink);
            }

            /**
             * @brief Sets the given node as right child.
             *
             * @param aLink Address of the Node to be linked to the right.
             */
            inline void _setRightChild(BTNode_t* aLink)
            {
               DNode_t::_setRightNode(aLink);
            }

            /**
             * @brief Constructor for TreeNode.
             *
             * @param data Data to store in the node.
             * @param llink Address of the node as the left child.
             * @param rlink Address of the node as the right child.
             */
            BinaryTreeNode(const type& data = static_cast<type>(NULL),
                  BTNode_t* llink = nullptr, BTNode_t* rlink = nullptr):
               DNode_t(data, llink, rlink){}

            /**
             * @brief Copy constructor.
             *
             * @param aNode A BinaryTreeNode.
             */
            BinaryTreeNode(const BTNode_t& aNode): DNode_t(aNode){}

            /**
             * @brief Move constructor.
             *
             * @param aNode Move reference to a BinaryTreeNode.
             */
            BinaryTreeNode(BTNode_t&& aNode): DNode_t(std::move(aNode)){}

            /**
             * @brief Destructor
             */
            virtual ~BinaryTreeNode()
            {
               if(leftChild()) { _setLeftChild(nullptr); }
               if(rightChild()) { _setRightChild(nullptr); }
            }

            /**
             * @brief Copy assignment operator overload.
             *
             * @param aNode A BinaryTreeNode object.
             */
            BTNode_t& operator=(const BTNode_t& aNode)
            {
               if(this != &aNode) { DNode_t::operator=(aNode); }
               return *this;
            }

            /**
             * @brief Move assignment operator overload.
             *
             * @param aNode Move reference to BinaryTreeNode object.
             */
            BTNode_t& operator=(BTNode_t&& aNode)
            {
               if(this != &aNode) { DNode_t::operator=(std::move(aNode)); }
               return *this;
            }

         public:
            /**
             * @brief Fethes the data stored in the node.
             *
             * @return Data stored in the node.
             */
            inline type& data() { return DNode_t::data(); }

            /**
             * @brief Fetches the address of the left child node.
             *
             * @return Address of the left child node.
             */
            inline const BTNode_t* leftChild()const
            {
               return static_cast<const BTNode_t*>(DNode_t::leftNode());
            }

            /**
             * @brief Fetches the address of the right child node.
             *
             * @return Address of the right child node.
             */
            inline const BTNode_t* rightChild()const
            {
               return static_cast<const BTNode_t*>(DNode_t::rightNode());
            }
      };
}

#endif
