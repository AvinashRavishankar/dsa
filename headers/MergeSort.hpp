/**
 * Copyright 2021 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file MergeSort.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated 2-way merge sort algorithm.
 */

#ifndef MERGESORT_INCLUDED
#define MERGESORT_INCLUDED

namespace dsa
   {
   /**
    * @brief A class for merge sort algorithm.
    */
   class MergeSort
   {
      private:

         /**
          * @brief Merges two consecutive sub-arrays in ascending order.
          *
          * @tparam ObjType Type of data structure.
          * @param outStruct Data structure to return as merged result.
          * @param array1_startIdx Starting index of the first array/list.
          * @param array1_endIdx Last index of the first array/list.
          * @param array2_endIdx Starting index of the second array/list.
          * @param inStruct Input data structure to merge.
          */
         template<typename ObjType>
            void _merge(ObjType& outStruct, const int& array1_startIdx,
                  const int& array1_endIdx, const int& array2_endIdx,
                  ObjType& inStruct)
            {
               using Iterator = typename ObjType::Iterator;

               Iterator array1_iter{inStruct.begin() + array1_startIdx};
               Iterator array2_iter{inStruct.begin() + array1_endIdx + 1};
               Iterator outIter{outStruct.begin() + array1_startIdx};

               const Iterator array2_startIter{array2_iter};
               const Iterator inStruct_endIter{inStruct.begin() +
                  array2_endIdx + 1};
               const Iterator outStruct_endIter{outStruct.begin() +
                  array2_endIdx + 1};

               while((array1_iter != array2_startIter) &&
                     (array2_iter != inStruct_endIter))
               {
                  if(*array1_iter <= *array2_iter)
                  {
                     *outIter = *array1_iter;
                     ++array1_iter;
                  }
                  else
                  {
                     *outIter = *array2_iter;
                     ++array2_iter;
                  }
                  ++outIter;
               }

               if(array1_iter == array2_startIter)
               {
                  while(outIter != outStruct.end())
                  {
                     *outIter = *array2_iter;
                     ++outIter;
                     ++array2_iter;
                  }
               }
               else
               {
                  while((outIter != outStruct_endIter) &&
                        (array1_iter != array2_startIter))
                  {
                     *outIter = *array1_iter;
                     ++outIter;
                     ++array1_iter;
                  }
               }
            }

         /**
          * @brief Executes a merge-pass through the data structure.
          *
          * @tparam ObjType Type of data structure.
          * @param outStruct Data structure to return after a merge pass.
          * @param length Length of the sub-list/sub=array to merge.
          * @param numRecords Number of records to merge.
          * @param inStruct Input data structure to merge.
          */
         template<typename ObjType>
         void _mPass(ObjType& outStruct, const int& length,
               const int& numRecords, ObjType& inStruct)
         {
            using Iterator = typename ObjType::Iterator;

            int index{0};
            while(index <= (numRecords - (2 * length)))
            {
               int array1_endIdx{index + length - 1};
               int array2_endIdx{index + (2 * length) - 1};

               _merge(outStruct, index, array1_endIdx,
                     array2_endIdx, inStruct);

               index += (2 * length);
            }

            // Merge remaining file of length < (2 * length)
            if((index + length - 1) < (numRecords - 1))
            {
               int subArray1_endIdx{index + length - 1};
               _merge(outStruct, index, subArray1_endIdx,
                     numRecords - 1, inStruct);
            }
            else
            {
               Iterator inStructIter{inStruct.begin() + index};
               Iterator outStructIter{outStruct.begin() + index};
               while(inStructIter != inStruct.end())
               {
                  *outStructIter = *inStructIter;
                  ++inStructIter;
                  ++outStructIter;
               }
            }
         }

      public:

         /**
          * @brief Operator() overload.
          *
          * @tparam ObjType Type of data structure.
          * @param aStruct Input data structure to sort.
          *
          * @return Merged data structure.
          */
         template<typename ObjType>
            ObjType operator()(ObjType& aStruct)
            {
               // Length of the sub-list/sub-array to merge.
               int length{1};

               int numRecords{aStruct.size()};
               ObjType outStruct{aStruct};

               while(length < numRecords)
               {
                  _mPass(outStruct, length, numRecords, aStruct);
                  length *= 2;
                  _mPass(aStruct, length, numRecords, outStruct);
                  length *= 2;
               }
               return outStruct;
            }
      };
   }

#endif
