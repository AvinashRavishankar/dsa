/*
 * Copyright 2021 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file HeapSort.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated max-heap sort algorithm.
 */

#ifndef HEAPSORT_INCLUDED
#define HEAPSORT_INCLUDED

namespace dsa
{
   /**
    * @brief A class for heap sort algorithm.
    */
   class HeapSort
   {

      private:

         /**
          * @brief Arranges the element in the given index to be
          * larger than it's children.
          *
          * @tparam ObjType Type of data structure.
          * @param aStruct Input data structure.
          * @param index Index of the element to start arrangement.
          * @param size Size of the data structure.
          */
         template<typename ObjType>
            void _sort(ObjType& aStruct, const int& index,
                  const int& size)
            {
               using Iterator = typename ObjType::Iterator;

               Iterator parent{aStruct.begin() + index};
               auto value{*parent};

               int childIdx{(index * 2) + 1};

               while(childIdx < size)
               {
                  Iterator child{aStruct.begin() + childIdx};

                  if(*child < *(child + 1))
                  {
                     ++childIdx;
                     ++child;
                  }

                  if(value >= *child) { break; }

                  *parent = *child;
                  parent = child;
                  childIdx = (childIdx * 2) + 1;
               }
               *parent = value;
            }

      public:

         /**
          * @brief Operator() overload.
          *
          * @tparam ObjType Type of data structure.
          * @param aStruct Input data structure to sort.
          */
         template<typename ObjType>
            void operator()(ObjType& aStruct)
            {
               using Iterator = typename ObjType::Iterator;

               // Convert aStruct into heap
               for(int i{(aStruct.size() / 2) - 1}; i >= 0; --i)
               {
                  _sort(aStruct, i, aStruct.size());
               }

               Iterator first{aStruct.begin()};

               // Loop to sort aStruct
               for(int i{aStruct.size() - 2}; i >= 0; --i)
               {
                  Iterator last{aStruct.begin() + (i + 1)};

                  // Interchange the first and the last elements of
                  // aStruct
                  auto temp{*last};
                  *last = *first;
                  *first = temp;

                  // Recreate heap
                  _sort(aStruct, 0, i);
               }
            }
   };
}
#endif
