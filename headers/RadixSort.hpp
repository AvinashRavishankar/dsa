/*
 * Copyright 2021 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file RadixSort.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated LSD radix-r sort algorithm.
 */

#ifndef RADIXSORT_INCLUDED
#define RADIXSORT_INCLUDED

#include <cmath>
#include "Array.hpp"
#include "LinkedQueue.hpp"

namespace dsa
{
   /**
    * @brief  LSD radix-10 sort algorithm class for integers.
    * @remark To be used with integers ONLY.
    */
   class RadixSort
   {

      public:
         /**
          * @brief Operator() overload.
          *
          * @tparam ObjType Type of data structure.
          * @param aStruct Input data structure to sort.
          * @param maxLen Maximum length of digits to sort in
          * the given data structure object.
          */
         template<typename ObjType>
            void operator()(ObjType& aStruct, const int& maxLen)
            {
               using Iterator = typename ObjType::Iterator;


               Array<LinkedQueue<int>, 10> bins;

               for(int i{1}; i <= maxLen; ++i)
               {
                  Iterator iter{aStruct.begin()};

                  // Fill the bins with array values.
                  while(iter != aStruct.end())
                  {
                     int quotient{*iter / 10};
                     int remainder{*iter % 10};
                     int index{remainder};

                     for(int j{i - 1}; j != 0 ; --j)
                     {
                        int temp_q{quotient};
                        quotient = temp_q / 10;
                        remainder = temp_q % 10;
                        index = remainder;
                     }
                     auto binIter{bins.begin() + index};
                     (*binIter).enqueue(*iter);
                     ++iter;
                  }

                  iter = aStruct.begin();
                  auto binIter{bins.begin()};

                  //Load values from bins to the array.
                  while(binIter != bins.end())
                  {
                     while(!(*binIter).isEmpty() &&
                           (iter != aStruct.end()))
                     {
                        *iter = (*binIter).front();
                        (*binIter).dequeue();
                        ++iter;
                     }
                     ++binIter;
                  }
               }
            }
   };

}
#endif
