/*
 * Copyright 2021 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file InsertionSort.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated insertion sort algorithm.
 */

#ifndef INSERTIONSORT_INCLUDED
#define INSERTIONSORT_INCLUDED

namespace dsa
{

   /**
    * @brief A class for insertion sort algorithm
    */
   class InsertionSort
   {
      private:

         /**
          * @brief Inserts the given element at the correct positon.
          *
          * @tparam ObjType Type/Class of the object to operate on.
          * @param aStruct Reference to a structure.
          * @param index Value at the index to be inserted.
          */
         template<typename ObjType>
            void _insert(ObjType& aStruct, const int& index)
            {
               using Iterator = typename ObjType::Iterator;
               Iterator startIter{aStruct.begin() + index};
               Iterator iter{startIter - 1};
               Iterator endIter{aStruct.rEnd()};

               auto value{*startIter};

               while(iter != endIter)
               {
                  if(value < *iter)
                  {
                     *(iter + 1) = *iter;
                     --iter;
                  }
                  else { break; }
               }

               if(iter == endIter)
               {
                  iter = aStruct.begin();
               }
               else { ++iter; }

               *iter = value;
            }

      public:

         /**
          * @brief () operator overload
          *
          * @tparam ObjType Type of the structure.
          * @param aStruct Data structure to run the insertion sort algorithm.
          * @remark Insertion sort does not support singly-linked lists.
          */
         template<typename ObjType>
            void operator()(ObjType& aStruct)
            {
               int size{aStruct.size()};
               for(int i{1}; i < size; ++i)
               {
                  _insert(aStruct, i);
               }
            }
   };
}

#endif
