/*
 * Copyright 2020 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file BinarySearchTree.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated binary search tree.
 */

#ifndef BINARYSEARCHTREE_INCLUDED
#define BINARYSEARCHTREE_INCLUDED

#include "BinaryTree.hpp"
#include "QuickSort.hpp"
#include <limits>

namespace
{

   /**
    * @brief A structure to store the weight, the root index, and the cost of
    * each element in the binary search tree.
    */
   typedef struct WRC
   {
      float w; /**< Weight of node element in the tree.*/
      int r;   /**< Index of the node element in the list.*/
      float c; /**< Cost of the node element in the list.*/

      /**
       * @brief Constructor
       */
      WRC(const float& weight, const int& rootIdx, const float& cost):
         w(weight), r(rootIdx), c(cost){}
   } WRC;

   /**
    * @brief To balance or not to balance.
    */
   enum Optim {
      STANDARD,   /**< Constructs an unbalanced tree.*/
      BALANCED    /**< Constructs a balanced tree.*/
      };
}

namespace dsa
{

   /**
    * @brief A template BinarySearchTree class.
    *
    * @tparam type Data type to store in BinarySearchTree.
    */
   template<typename type>
      class BinarySearchTree: public BinaryTree<type>
      {
         private:

            using List_t  = DoubleList<type>;
            using BTree_t = BinaryTree<type>;
            using Node_t  = BinaryTreeNode<type>;
            using BST_t   = BinarySearchTree<type>;
            using DList_t = DoubleList< DoubleList<WRC> >;

            /**
             * @brief Computes the minimum cost the tree for all elements in the
             * given list.
             *
             * @tparam ObjType Type of list/array as argument.
             * @param aList A list/array object.
             *
             * @return A list of type @code DoubleList<DoubleList<WRC> > @endcode
             */
            template<typename ObjType>
            DList_t* _computeCostMatrix(ObjType& aList)
            {

               using Iterator = typename ObjType::Iterator;

               Iterator listBegin{aList.begin()};
               Iterator listEnd{aList.end()};

               DList_t* wrc_ij{new DList_t}; // Where j <= i

               if (listBegin != listEnd)
               {
                  int size(aList.size());

                  // Set probabilities for elements found / not found in the BST
                  DoubleList<float> probsFound, probsNotFound;
                  for(int i{0}; i < size; ++i)
                  {
                     probsFound.insert(1 / static_cast<float>(size));
                     probsNotFound.insert(1 / static_cast<float>(size));
                  }
                  probsNotFound.insert(1 / static_cast<float>(size));

                  for(int i{0}; i < size ; ++i)
                  {
                     (*wrc_ij).insert(DoubleList<WRC>());
                     for(int j{i}; j <= (i + 1); ++j)
                     {
                        if(i == j)
                        {
                           // Optimal trees with zero nodes.
                           (*wrc_ij)[i].insert({probsNotFound[i], 0, 0.0f});
                        }
                        else
                        {
                           // Optimal trees with one node.
                           (*wrc_ij)[i].insert({probsNotFound[i] +
                                 probsNotFound[j] + probsFound[i], j,
                                 probsNotFound[i] + probsNotFound[j] +
                                 probsFound[i]});
                        }
                     }
                  }
                  (*wrc_ij).insert(DoubleList<WRC>());
                  (*wrc_ij)[size].insert({probsNotFound[size], 0, 0.0f});

                  // Find optimal trees with m nodes.
                  for(int m{2}; m <= size; ++m)
                  {
                     for(int i{0}; i <= (size - m); ++i)
                     {
                        int j{i + m};

                        int r_ij{(*wrc_ij)[i][j - i - 1].r};

                        float w_ij{(*wrc_ij)[i][j - i - 1].w
                           + probsFound[j - 1] + probsNotFound[j]};

                        float c_ij{std::numeric_limits<float>::max()};

                        // Compute the index of the root element with minimum
                        // cost of both the left and the right sub-trees.
                        for(int l{i + 1}; l <= j; ++l)
                        {
                           float minC = (*wrc_ij)[i][l - i - 1].c
                              + (*wrc_ij)[l][j - l].c;

                           if(c_ij > minC)
                           {
                             c_ij = minC;
                             r_ij = l;
                           }
                        }

                        c_ij += w_ij;
                        (*wrc_ij)[i].insert({w_ij, r_ij, c_ij});
                     }
                  }
               }
               return wrc_ij;
            }

         protected:

            /**
             * @brief Inserts the given data at the proper location in the tree.
             *
             * @param aNode Pointer to a BinaryTreeNode instance.
             * @param aData A Data to insert.
             * @return Pointer to the node after insertion.
             */
            Node_t* _insert(Node_t* aNode, const type& aData)
            {
               Node_t* retNode;
               if(!aNode) { retNode = new Node_t{aData}; }
               else if(aData < aNode->data())
               {
                  aNode->_setLeftChild(_insert(aNode->_leftChild(), aData));
                  retNode = aNode;
               }
               else if(aData > aNode->data())
               {
                  aNode->_setRightChild(_insert(aNode->_rightChild(), aData));
                  retNode = aNode;
               }
               else
               {
                  std::invalid_argument("value exists already !!");
               }
               return retNode;
            }

            /**
             * @brief Fetches the min node in the tree.
             *
             * @param aNode Pointer to a node to begin search.
             * @return Pointer to the node with the minimum value.
             */
            Node_t* _minNode(Node_t* aNode)
            {
               Node_t* min{aNode};
               if(min->_leftChild())
               {
                  min = _minNode(min->_leftChild());
               }
               return min;
            }

            /**
             * @brief Fetches the max node in the given tree.
             *
             * @param aNode Pointer to a node to begin search.
             * @return Pointer to the node with maximum value.
             */
            Node_t* _maxNode(Node_t* aNode)
            {
               Node_t* max{aNode};
               if(max->_rightChild())
               {
                  max = _maxNode(max->_rightChild());
               }
               return max;
            }

            /**
             * @brief Deletes the node with the given key.
             *
             * @param aKey A key to search in the tree.
             * @param aNode A BinaryTreeNode to start the search.
             * @return Pointer to the starting node after deletion.
             */
            Node_t* _deleteNode(const type& aKey, Node_t* aNode)
            {
               if(aNode)
               {
                  if(aKey == aNode->data())
                  {
                     if(aNode->rightChild())
                     {
                        Node_t* min{_minNode(aNode->_rightChild())};
                        aNode->data() = min->data();
                        aNode->_setRightChild(
                              _deleteNode(min->data(), aNode->_rightChild()));
                     }
                     else if(aNode->leftChild())
                     {
                        Node_t* max{_maxNode(aNode->_leftChild())};
                        aNode->data() = max->data();
                        aNode->_setLeftChild(
                              _deleteNode(max->data(), aNode->_leftChild()));
                     }
                     else
                     {
                        delete aNode;
                        aNode = nullptr;
                     }
                  }
                  else if(aKey < aNode->data())
                  {
                     aNode->_setLeftChild(
                           _deleteNode(aKey, aNode->_leftChild()));
                  }
                  else
                  {
                     aNode->_setRightChild(
                           _deleteNode(aKey, aNode->_rightChild()));
                  }
               }
               return aNode;
            }

         public:

            /**
             * @brief Constructor.
             */
            BinarySearchTree(): BTree_t() {}

            /**
             * @brief Construct a BinarySearchTree from a list/array.
             *
             * The constructs sorts the given list and computes a cost matrix
             * with equal probabilities to choose the optimal root node for a
             * balanced BinarySearchTree.
             *
             * @tparam ObjType Type of Array/List.
             * @param aList A list/array object.
             * @param opt Optimization strategy: STANDARD, BALANCED.
             */
            template<typename ObjType>
            BinarySearchTree(ObjType& aList, const Optim& opt = STANDARD):
               BTree_t()
            {
               int rootIdx{aList.size() / 2};

               if(opt == BALANCED)
               {
                  QuickSort qSort;
                  qSort(aList);
                  DList_t* wrc{_computeCostMatrix(aList)};
                  rootIdx = (*wrc)[0][aList.size()].r - 1;
                  delete wrc;
               }

               BTree_t::_setRoot(_insert(BTree_t::_root(), aList[rootIdx]));

               for(int i{0}; i < aList.size(); ++i)
               {
                  _insert(BTree_t::_root(), aList[i]);
               }
            }

            /**
             * @brief Copy constructor.
             *
             * @param aBST A BinarySearchTree object.
             */
            BinarySearchTree(const BST_t& aBST): BTree_t(aBST) {}

            /**
             * @brief Move constructor.
             *
             * @param aBST Move reference to a BinarySearchTree object.
             */
            BinarySearchTree(BST_t&& aBST): BTree_t(std::move(aBST)) {}

            /**
             * @brief Destructor
             */
            ~BinarySearchTree() {}

            /**
             * @brief Copy assignment operator overload
             *
             * @param aBST A BinarySearchTree object.
             */
            BST_t& operator=(const BST_t& aBST)
            {
               BTree_t::operator=(aBST);
               return *this;
            }

            /**
             * @brief Move assignment operator overload.
             *
             * @param aBST Move reference to a BinarySearchTree object.
             */
            BST_t& operator=(BST_t&& aBST)
            {
               BTree_t::operator=(std::move(aBST));
               return *this;
            }

            /**
             * @brief Inserts the given data to the tree.
             *
             * @param aData A data to insert.
             */
            void insert(const type& aData)
            {
               if(!BTree_t::_root())
               {
                  BTree_t::_setRoot(_insert(BTree_t::_root(), aData));
               }
               else { _insert(BTree_t::_root(), aData); }
            }

            /**
             * @brief Fetches the min element.
             *
             * @return Minimum element in the tree.
             */
            type getMin()
            {
               Node_t* min{_minNode(BTree_t::_root())};
               return min->data();
            }

            /**
             * @brief Fetches the max element.
             *
             * @return Max element in the tree.
             */
            type getMax()
            {
               Node_t* max{_maxNode(BTree_t::_root())};
               return max->data();
            }

            /**
             * @brief Removes the node with the given key.
             *
             * @param aKey A key to search in the tree.
             */
            void remove(const type& aKey)
            {
               BTree_t::_setRoot(_deleteNode(aKey, BTree_t::_root()));
            }
      };
}

#endif
