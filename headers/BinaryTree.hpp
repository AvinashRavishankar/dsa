/*
 * Copyright 2020 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file BinaryTree.hpp
 * @author Avinash Chikkadlur Ravi Shanakar
 * @brief Header file for templated binary tree.
 */

#ifndef BINARYTREE_INCLUDED
#define BINARYTREE_INCLUDED

#include <stdexcept>
#include "DoubleList.hpp"
#include "BinaryTreeNode.hpp"

namespace dsa
{
   /**
    * @brief A template class to store a binary tree structure.
    *
    * @tparam type Data type to store in the Tree structure.
    */
   template<typename type>
      class BinaryTree
      {
         private:

            using BTree_t = BinaryTree<type>;
            using DList_t = DoubleList<type>;
            using Node_t  = BinaryTreeNode<type>;

            Node_t* _r; ///< Member pointer to the root BinaryTreeNode.

         protected:

            /**
             * @brief Fetches the root node.
             *
             * @return Pointer to the root node.
             */
            inline Node_t* _root() { return _r; }

            /**
             * @brief Sets the given node as root.
             *
             * @param aNode Pointer to a BinaryTreeNode.
             */
            inline void _setRoot(Node_t* aNode) { _r = aNode; }

            /**
             * @brief Clones an entire tree starting from the given
             * BinaryTreeNode traversing recursively in post-order.
             *
             * @param aNode A root node to traverse from.
             * @return Pointer to the root BinaryTreeNode.
             */
            Node_t* _copy(Node_t* aNode)
            {
               if(aNode)
               {
                  Node_t* lLink{_copy(aNode->_leftChild())};
                  Node_t* rLink{_copy(aNode->_rightChild())};
                  return new Node_t{aNode->data(), lLink, rLink};
               }
               else { return nullptr; }
            }

            /**
             * @brief Deletes the entire tree starting from the given
             * BinaryTreeNode traversing recursively in post-order.
             *
             * @param aNode A root node to traverse from.
             * @return nullptr.
             */
            Node_t* _delete(Node_t* aNode)
            {
               if(aNode)
               {
                  _delete(aNode->_leftChild());
                  _delete(aNode->_rightChild());
                  delete aNode;
                  return nullptr;
               }
               else { return nullptr; }
            }

            /**
             * @brief Traverses the tree in-order.
             *
             * @param aNode Pointer to a node to start traversal.
             * @param aDList A DoubleList object to store the data as traversed.
             */
            void _traverseInOrder(Node_t* aNode, DList_t& aDList)
            {
               if(aNode)
               {
                  _traverseInOrder(aNode->_leftChild(), aDList);
                  aDList.insert(aNode->data());
                  _traverseInOrder(aNode->_rightChild(), aDList);
               }
            }

            /**
             * @brief Traverses the tree in pre-order.
             *
             * @param aNode Pointer to a node to start traversal.
             * @param aDList A DoubleList object to store the data as traversed.
             */
            void _traversePreOrder(Node_t* aNode, DList_t& aDList)
            {
               if(aNode)
               {
                  aDList.insert(aNode->data());
                  _traversePreOrder(aNode->_leftChild(), aDList);
                  _traversePreOrder(aNode->_rightChild(), aDList);
               }
            }

            /**
             * @brief Traverses the tree in post-order.
             *
             * @param aNode Pointer to a node to start traversal.
             * @param aDList A DoubleList object to store the data as traversed.
             */
            void _traversePostOrder(Node_t* aNode, DList_t& aDList)
            {
               if(aNode)
               {
                  _traversePostOrder(aNode->_leftChild(), aDList);
                  _traversePostOrder(aNode->_rightChild(), aDList);
                  aDList.insert(aNode->data());
               }
            }

            /**
             * @brief Compares if two trees are equal in pre-order.
             *
             * @param aNode Pointer to a BinaryTreeNode to compare.
             * @param anotherNode Pointer to a BinaryTreeNode to compare
             * @return True if equal, else false.
             */
            static bool _isEqual(Node_t* aNode, Node_t* anotherNode)
            {
               bool isEqual{false};

               if(!aNode && !anotherNode) { isEqual = true; }
               else if(aNode && anotherNode)
               {
                  if(aNode->data() == anotherNode->data())
                  {
                     isEqual = _isEqual(aNode->_leftChild(),
                           anotherNode->_leftChild());

                     if(isEqual)
                     {
                        isEqual = _isEqual(aNode->_rightChild(),
                                 anotherNode->_rightChild());
                     }
                  }
               }
               return isEqual;
            }

         public:

            /**
               * @brief Constructor.
               */
            BinaryTree(): _r(nullptr){}

            /**
             * @brief Constructor.
             *
             * @param root Pointer to a node to set as root.
             */
             BinaryTree(Node_t* root): _r(root){}

            /**
             * @brief Copy constructor.
             *
             * @param aBTree A BinaryTree object.
             */
            BinaryTree(const BTree_t& aBTree):
               _r(nullptr) { _r = _copy(aBTree._r); }

            /**
             * @brief Move constructor.
             *
             * @param aBTree Move reference to a BinaryTree object.
             */
            BinaryTree(BTree_t&& aBTree):
               _r(aBTree._r) { aBTree._r = nullptr; }

            /**
             * @brief Destructor.
             */
            virtual ~BinaryTree()
            {
               if(_r) { _r = _delete(_r); }
            }

            /**
             * @brief Copy assignment operator overload.
             *
             * @param aBTree A BinaryTree object.
             */
            BTree_t& operator=(const BTree_t& aBTree)
            {
               if(this != &aBTree)
               {
                  _r = _delete(_r);
                  _r = _copy(aBTree._r);
               }
               return *this;
            }

            /**
             * @brief Move assignment operator overload.
             *
             * @param aBTree Move reference to a BinaryTree object.
             */
            BTree_t& operator=(BTree_t&& aBTree)
            {
               if(this != &aBTree)
               {
                  _r = _delete(_r);
                  _r = aBTree._r;
                  aBTree._r = nullptr;
               }
               return *this;
            }

            /**
             * @brief Checks if the tree is empty.
             *
             * @return True if empty. Else, false.
             */
            bool isEmpty() { return !_r; }

            /**
             * @brief Gets the value at the root node.
             *
             * @param aBTree Pointer to a binary tree.
             * @return Data at the root node.
             */
            type root()const { return _r->data(); }

            /**
             * @brief Inserts the given data as the root node of the tree.
             *
             * @param aData Data to insert.
             * @param leftTree Pointer to a tree to insert as left sub-tree.
             * @param rightTree Pointer to a tree to insert as right sub-tree.
             * @return BinaryTree object
             * @remark BinaryTree passed as arguments are emptied after this
             * operation.
             */
            static BTree_t insert(const type& aData,
                  BTree_t* leftTree = nullptr,
                  BTree_t* rightTree = nullptr)
            {
               if(leftTree && rightTree)
               {
                  BTree_t bTree{new Node_t{aData, leftTree->_r, rightTree->_r}};
                  leftTree->_r = nullptr;
                  rightTree->_r = nullptr;
                  return bTree;
               }
               else if(leftTree)
               {
                  BTree_t bTree{new Node_t{aData, leftTree->_r, nullptr}};
                  leftTree->_r = nullptr;
                  return bTree;
               }
               else if(rightTree)
               {
                  BTree_t bTree{new Node_t{aData, nullptr, rightTree->_r}};
                  rightTree->_r = nullptr;
                  return bTree;
               }
               else
               {
                  BTree_t bTree{new Node_t{aData}};
                  return bTree;
               }
            }

            /**
             * @brief Traverses the tree in-order.
             *
             * @return A DoubleList object with entries as traversed.
             */
            DList_t traverseInOrder()
            {
               DList_t aList;
               _traverseInOrder(_r, aList);
               return aList;
            }

            /**
             * @brief Traverses the tree in pre-order.
             *
             * @return A DoubleList object with entries as traversed.
             */
            DList_t traversePreOrder()
            {
               DList_t aList;
               _traversePreOrder(_r, aList);
               return aList;
            }

            /**
             * @brief Traverses the tree in post-order.
             *
             * @return A DoubleList object with entries as traversed.
             */
            DList_t traversePostOrder()
            {
               DList_t aList;
               _traversePostOrder(_r, aList);
               return aList;
            }

            /**
             * @brief Compares to check if two trees are identical with
             * pre-order traversal.
             *
             * @param aBTree A BinaryTree object.
             * @param anotherBTree Another BinaryTree object to compare with.
             * @return True if tree are equal. Else, false.
             */
            static bool areEqual(BTree_t& aBTree, BTree_t& anotherBTree)
            {
               return _isEqual(aBTree._r, anotherBTree._r);
            }
      };
}

#endif
