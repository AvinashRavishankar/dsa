/*
 * Copyright 2020 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file LinkedStack.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for linked-list based templated queue.
 */

#ifndef LINKEDSTACK_INCLUDED
#define LINKEDSTACK_INCLUDED

#include <utility>
#include "SingleList.hpp"

namespace dsa
{
   /**
    * @brief A template stack class constructed with single-linked list.
    *
    * @tparam type Type of data to store in the Stack.
    */
   template<typename type>
      class LinkedStack: private SingleList<type>
      {
         private:

            using Node_t  = Node<type>;
            using List_t  = SingleList<type>;
            using Stack_t = LinkedStack<type>;

         public:

            /**
             * @brief Default constructor.
             */
            LinkedStack(): List_t(){}

            /**
             * @brief Copy constructor.
             *
             * @param aStack A LinkedStack object.
             */
            LinkedStack(const Stack_t& aStack): List_t(aStack){}

            /**
             * @brief Move constructor.
             *
             * @param aStack Move reference to a LinkedStack object.
             */
            LinkedStack(Stack_t&& aStack): List_t(std::move(aStack)){}

            /**
             * @brief Destructor.
             */
            ~LinkedStack(){}

            /**
             * @brief Copy assignment operator overload.
             *
             * @param aStack A LinkedStack object.
             */
            Stack_t& operator=(const Stack_t& aStack)
            {
               if(this != &aStack)
               {
                  List_t::operator=(aStack);
               }
               return *this;
            }

            /**
             * @brief Move assignment operator overload.
             *
             * @param aStack Move reference to a LinkedStack object.
             */
            Stack_t& operator=(Stack_t&& aStack)
            {
               if(this != &aStack)
               {
                  List_t::operator=(std::move(aStack));
               }
               return *this;
            }

            /**
             * @brief Checks if the given stack is empty.
             *
             * @return True if empty. Else false.
             */
            bool isEmpty() const { return List_t::isEmpty(); }

            /**
             * @brief Fetches the top element in the stack.
             *
             * @return Top element of the stack.
             */
            type top() const
            {
               if(isEmpty())
               {
                  throw std::logic_error("empty Stack!!");
               }
               else
               {
                  return List_t::operator[](0);
               }
            }

            /**
             * @brief Pushes data into the stack.
             *
             * @param aData Data to push into the stack.
             */
            void push(const type& aData)
            {
               List_t::insertAt(0, aData);
            }

            /**
             * @brief Pops the top element out of the stack.
             */
            void pop()
            {
               try
               {
                  List_t::removeAt(0);
               }
               catch(std::logic_error& e)
               {
                  throw std::logic_error("empty Stack!!");
               }
            }
      };
}

#endif
