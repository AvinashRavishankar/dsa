/*
 * Copyright 2020 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file Stack.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated stack class.
 */

#ifndef STACK_INCLUDED
#define STACK_INCLUDED

#include <cstring>
#include <stdexcept>

namespace dsa
{
   /**
    * @brief A template Stack class using heap array.
    *
    * @tparam type Type of data to store in the Stack.
    * @tparam length Size of the Stack.
    */
   template<typename type, int length>
      class Stack{

         private:

            using Stack_t = Stack<type, length>;
            int _top; /**< Member variable to store the index of the top
                        of the stack.*/
            type* _m; /**< Pointer member to hold an allocated array on
                        the heap.*/

         public:

            /**
             * @brief Constructor.
             */
            Stack(): _top(-1), _m(new type[length]){}

            /**
             * @brief Copy constructor.
             *
             * @param aStack A Stack object.
             *
             * Crates a deep copy of the given Stack.
             */
            Stack(const Stack_t& aStack): _top(aStack._top), _m(nullptr)
         {
            _m = new type[length];
            std::memcpy(_m, aStack._m, sizeof(type) * length);
         }

            /**
             * @brief Move Constructor
             *
             * @param aStack Move refernce to a Stack object.
             */
            Stack(Stack_t && aStack): _top(-1), _m(nullptr)
         {
            _m = aStack._m;
            _top = aStack._top;

            aStack._top = -1;
            aStack._m = nullptr;
         }

            /**
             * @brief Destructor
             */
            ~Stack()
            {
               delete[] _m;
            }

            /**
             * @brief Copy assignment operation overload.
             *
             * @param aStack A Stack object.
             * @return Refrence to the current Stack object after copy.
             */
            Stack_t& operator=(const Stack_t& aStack)
            {
               if(this != &aStack)
               {
                  std::memcpy(_m, aStack._m, sizeof(type) * length);
                  _top = aStack._top;
               }
               return *this;
            }

            /**
             * @brief Move assigment operator overload.
             *
             * @param aStack Move reference to a Stack object.
             * @return Reference to the current Stack object after move.
             */
            Stack_t& operator=(Stack_t&& aStack)
            {
               if(this != &aStack)
               {
                  delete[] _m;

                  _top = aStack._top;
                  _m = aStack._m;

                  aStack._top = -1;
                  aStack._m = nullptr;
               }
               return *this;
            }

            /**
             * @brief Checks if the Stack is empty.
             *
             * @return true, if empty. Else, false.
             */
            inline bool isEmpty() const { return _top == -1; }

            /**
             * @brief Return the top element in the Stack.
             *
             * @return Top element of the given Stack object.
             */
            type top() const
            {
               if(!_m)
               {
                  throw std::logic_error("trying to dereference a nullptr!!");
               }
               else if(isEmpty())
               {
                  throw std::logic_error("empty stack!!");
               }
               else
               {
                  return _m[_top];
               }
            }

            /**
             * @brief Pushes an element into Stack.
             *
             * @param item Element to be pushed into the Stack.
             */
            void push(const type& item)
            {
               if(!_m)
               {
                  throw std::logic_error("trying to dereference a nullptr!!");
               }
               else if(_top >= (length - 1))
               {
                  throw std::out_of_range("exceeds stack size!!");
               }
               else
               {
                  _top += 1;
                  _m[_top] = item;
               }
            }

            /**
             * @brief Pops an element from the Stack.
             *
             */
            void pop()
            {
               if(_top == -1)
               {
                  throw std::out_of_range("stack already empty!!");
               }
               else
               {
                  _m[_top] = static_cast<type>(NULL);
                  _top -= 1;
               }
            }
      };
}

#endif
