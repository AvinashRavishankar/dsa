/*
 * Copyright 2020 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file SingleList.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated, singly-linked list.
 */

#ifndef SINGLELIST_INCLUDED
#define SINGLELIST_INCLUDED

#include "Node.hpp"
#include <stdexcept>

namespace dsa
{
   /**
    * @brief A template class for singly-linked list.
    *
    * @tparam type Type of data to store in the linked list.
    *
    * @todo Add function to invert list.
    * @todo Add function to concatenate list.
    * @todo Add tail pointer to keep track of the end of the list.
    * @todo Use added tail pointer to improve insert() from O(n)->O(0).
    * @todo Single-list should be modified with unique pointer to implement
    * concatenation. Without it, deleting THIS list will leave the argument list
    * with a dangling pointer.
    */
   template<typename type>
      class SingleList
      {

         private:

            using List_t = SingleList<type>;
            using Node_t = Node<type>;

            Node_t* _h; /**< Pointer member to hold the head node of the list.*/
            int _size;  /**< Member variable to store the size of the list.*/

            /**
             * @brief Deletes nodes recursively.
             */
            void _deleteNodes()
            {
               while(_h->_node())
               {
                  Node_t* toDelete{_h};
                  _h = _h->_node();
                  delete toDelete;
                  --_size;
               }

               delete _h;
               --_size;

               _h = nullptr;

               if(_size != 0)
               {
                  std::logic_error("memory leak!! Not all nodes deleted!!");
               }
            }

            /**
             * @brief Clones all nodes linked to the node in the given argument
             * and assigns it to the head pointer (_h) of this class.
             *
             * @param startNode A Node to start cloning.
             */
            void _copyNodes(Node_t* startNode)
            {
               if(startNode)
               {
                  _h = new Node_t(*startNode);

                  Node_t* origin{startNode};
                  Node_t* copy{_h};

                  int index{1};
                  while(index < _size)
                  {
                     origin = origin->_node();
                     copy->_setNode(new Node_t(*origin));
                     copy = copy->_node();
                     ++index;
                  }
               }
               else { _h = nullptr; }
            }

         public:

            /**
             * @brief Iterator class.
             */
            class Iterator
            {
               private:

                  Node_t* _currentElement; /**< Pointer to the current list
                                           element. */

               public:

                  /**
                   * @brief Constructor
                   *
                   * @param aPointer A pointer to initialize the iterator.
                   */
                  Iterator(Node_t* aPointer=nullptr):
                     _currentElement(aPointer){}

                  /**
                   * @brief Copy constructor.
                   *
                   * @param anIterator Reference to an Array iterator.
                   */
                  Iterator(const Iterator& anIterator):
                     _currentElement(anIterator._currentElement){}

                  /**
                   * @brief Move constructor.
                   *
                   * @param anIterator Move reference to an Array iterator.
                   */
                  Iterator(Iterator&& anIterator):
                     _currentElement(anIterator._currentElement)
               {
                  anIterator._currentElement = nullptr;
               }

                  /**
                   * @brief Copy assignment operation.
                   *
                   * @param anIterator Reference to an Array iterator.
                   * @return An Iterator.
                   */
                  Iterator& operator=(const Iterator& anIterator)
                  {
                     if(this != &anIterator)
                     {
                        _currentElement = anIterator._currentElement;
                     }
                     return *this;
                  }

                  /**
                   * @brief Move assignment operation.
                   *
                   * @param anIterator Reference to an Array iterator.
                   * @return An Iterator.
                   */
                  Iterator& operator=(Iterator&& anIterator)
                  {
                     if(this != &anIterator)
                     {
                        _currentElement = anIterator._currentElement;
                        anIterator._currentElement = nullptr;
                     }
                     return *this;
                  }

                  /**
                   * @brief Binary + operator overload.
                   *
                   * @param value Amount by which the iterator is incremented.
                   * @return An iterator incremented by the given value.
                   */
                  inline Iterator operator+(int value)
                  {
                     Node_t* newElement{_currentElement};
                     for(int i{0}; i < value; ++i)
                     {
                        newElement = newElement->_node();
                     }
                     return Iterator(newElement);
                  }

                  /**
                   * @brief Binary += operator overload.
                   *
                   * @param value Amount by which the iterator is incremented.
                   */
                  inline Iterator& operator+=(int value)
                  {
                     for(int i{0}; i < value; ++i)
                     {
                        _currentElement = _currentElement->_node();
                     }
                     return *this;
                  }

                  /**
                   * @brief Unary prefix ++ operator overload.
                   *
                   * @return Iterator
                   */
                  inline Iterator& operator++()
                  {
                     _currentElement = _currentElement->_node();
                     return *this;
                  }

                  /**
                   * @brief Unary postfix ++ operator overload.
                   *
                   * @return Iterator
                   */
                  inline Iterator operator++(int)
                  {
                     Iterator old{*this};
                     operator++();
                     return old;
                  }

                  /**
                   * @brief Dereference operator overload.
                   *
                   * @return Value at the dereferenced element.
                   */
                  inline type& operator*() const
                  {
                     return _currentElement->data();
                  }

                  /**
                   @brief Binary == operator overload
                   *
                   * @param anIterator Reference to an iterator.
                   * @return True if both iterators point to the same element.
                   * Else, false.
                   */
                  inline bool operator==(const Iterator& anIterator)
                  {
                     return _currentElement == anIterator._currentElement;
                  }

                  /**
                   * @brief Binary != operator overload
                   *
                   * @param anIterator Reference to an iterator.
                   * @return True if both iterators point to different elements.
                   * Else, false.
                   */
                  inline bool operator!=(const Iterator& anIterator)
                  {
                     return _currentElement != anIterator._currentElement;
                  }
           };

            /**
             * @brief Default constructor.
             */
            SingleList(): _h(nullptr), _size(0){}

            /**
             * @brief Copy constructor.
             *
             * @param aList A SingleList object.
             */
            SingleList(const List_t& aList): _size(aList._size)
         {
            _copyNodes(aList._h);
         }

            /**
             * @brief Move constructor.
             *
             * @param aList Move reference to a SingleList object.
             */
            SingleList(List_t&& aList): _h(aList._h), _size(aList._size)
         {
            aList._h = nullptr;
            aList._size = 0;
         }

            /**
             * @brief Destructor.
             */
            virtual ~SingleList()
            {
               if(_h) { _deleteNodes(); }
            }
            /**
             * @brief Copy assignment operator overload.
             *
             * @param aList A SingleList object.
             */
            List_t& operator=(const List_t& aList)
            {
               if(this != &aList)
               {
                  if(_h) { _deleteNodes(); }

                  _size = aList._size;
                  _copyNodes(aList._h);
               }
               return *this;
            }

            /**
             * @brief Move assignment operator overload.
             *
             * @param aList Move reference to a SingleList object.
             */
            List_t& operator=(List_t&& aList)
            {
               if(this != &aList)
               {
                  if(_h) { _deleteNodes(); }

                  _h = aList._h;
                  _size = aList._size;

                  aList._h = nullptr;
                  aList._size = 0;
               }
               return *this;
            }

            /**
             * Returns the size of the list.
             *
             * @return Size of the list.
             */
            inline int size()const { return _size; }

            /**
             * @brief Checks if the given list is empty.
             *
             * @return True if empty. Else false.
             */
            inline bool isEmpty()const { return !_h; }

            /**
             * @brief Subscript operator overload.
             *
             * @param index Index of the list to fetch.
             * @return Data at the given index.
             */
            type& operator[](const int& index)const
            {
               if(isEmpty())
               {
                  throw std::logic_error("empty list!!");
               }
               else if((index < 0) || (index >= _size))
               {
                  throw std::out_of_range("index out of list range!!");
               }
               else
               {
                  Node_t* toFetch{_h};
                  for(int pos{0}; pos < index; ++pos)
                  {
                     toFetch = toFetch->_node();
                  }
                  return toFetch->data();
               }
            }

            /**
             * @brief Appends data to the end of the list.
             *
             * @param aData Data to append.
             */
            void insert(const type& aData)
            {
               if(isEmpty())
               {
                  _h = new Node_t{aData};
               }
               else
               {
                  Node_t* last{_h};
                  while(last->_node())
                  {
                     last = last->_node();
                  }
                  last->_setNode(new Node_t{aData});
               }
               ++_size;
            }

            /**
             * @brief Inserts data at the given index.
             *
             * @param index Index of the list to insert the data.
             * @param aData Data to insert.
             */
            void insertAt(const int& index, const type& aData)
            {
               if(isEmpty() || (index >= _size))
               {
                  insert(aData);
               }
               else if(index == 0)
               {
                  _h = new Node_t{aData, _h};
                  ++_size;
               }
               else
               {
                  Node_t* previousNode{_h};
                  for(int pos{0}; pos < (index - 1); ++pos)
                  {
                     previousNode = previousNode->_node();
                  }
                  previousNode->_setNode(
                        new Node_t{aData, previousNode->_node()});
                  ++_size;
               }
            }

            /**
             * @brief Removes the last element from the list.
             */
            void remove()
            {
               if(isEmpty())
               {
                  throw std::logic_error("empty list!!");
               }
               else if(!_h->_node())
               {
                  delete _h;
                  _h = nullptr;
                  --_size;
               }
               else
               {
                  Node_t* last{_h};
                  while((last->_node())->_node())
                  {
                     last = last->_node();
                  }
                  delete last->_node();
                  last->_setNode(nullptr);
                  --_size;
               }
            }

            /**
             * @brief Removes the element at the given index of the given list.
             *
             * @param index Index of the list to remove the data.
             */
            void removeAt(const int& index)
            {
               if(isEmpty())
               {
                  throw std::logic_error("empty list!!");
               }
               else if((index < 0) || (index >= _size))
               {
                  throw std::out_of_range("index out of list range!!");
               }
               else if(index == 0)
               {
                  Node_t* toDelete{_h};
                  _h = _h->_node();
                  delete toDelete;
                  --_size;
               }
               else
               {
                  Node_t* previousNode{_h};

                  for(int pos{0}; pos < (index - 1); ++pos)
                  {
                     previousNode = previousNode->_node();
                  }

                  Node_t* toDelete{previousNode->_node()};
                  previousNode->_setNode(toDelete->_node());
                  delete toDelete;
                  --_size;
               }
            }

            /**
             * @brief Returns an Iterator to the start of the array.
             *
             * @return Iterator to the start of the array.
             */
            inline Iterator begin() { return Iterator(_h); }

            /**
             * @brief Returns an Iterator to the end of the array.
             *
             * @return Iterator to the end of the array.
             */
            inline Iterator end() { return Iterator(); }
      };
}

#endif
