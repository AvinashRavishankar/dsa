/*
 * Copyright 2021 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file QuickSort.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated quick sort algorithm.
 */

#ifndef QUICKSORT_INCLUDED
#define QUICKSORT_INCLUDED

namespace dsa
{

   /**
    * @brief A class for quick sort algorithm.
    */
   class QuickSort
   {
      private:

         /**
          * @brief Sorts the given template Array object.
          *
          * @tparam ObjType Type of data structure.
          * @param aStruct Data structure to quick.
          * @param startIndex index to start the sort.
          * @param endIndex index to end the sort.
          */
         template<typename ObjType>
            void _qsort(ObjType& aStruct, const int& startIndex,
                  const int& endIndex)
            {
               using Iterator = typename ObjType::Iterator;
               if(startIndex < endIndex)
               {
                  int i{startIndex};
                  int j{endIndex};

                  Iterator startIter{aStruct.begin() + startIndex};
                  auto value{*startIter};

                  Iterator iterI{aStruct.begin() + i};
                  Iterator iterJ{aStruct.begin() + j};

                  while(true)
                  {
                     while(*iterI <= value)
                     {
                        ++i;
                        ++iterI;
                        if(i == endIndex) { break; }
                     }

                     while(*iterJ >= value)
                     {
                        --j;
                        --iterJ;
                        if(j == startIndex) { break; }
                     }

                     if(i < j)
                     {
                        // Swap
                        auto temp{*iterI};
                        *iterI = *iterJ;
                        *iterJ = temp;
                     }
                     else { break; }
                  }
                  // Swap
                  auto temp{*startIter};
                  *startIter = *iterJ;
                  *iterJ = temp;

                  // Sort LHS
                  _qsort(aStruct, startIndex, j - 1);
                  // Sort RHS
                  _qsort(aStruct, j + 1, endIndex);
               }
            }

      public:

         /**
          * @brief () operator overload
          *
          * @tparam ObjType Type of data structure.
          * @param aStruct Data structure to run the quick sort algorithm.
          * @remark Quick sort does not support singly-linked lists.
          */
         template<typename ObjType>
            void operator()(ObjType& aStruct)
            {
               _qsort(aStruct, 0, aStruct.size() - 1);
            }
   };
}

#endif
