/*
 * Copyright 2020 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file Queue.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated Queue class
 */

#ifndef QUEUE_INCLUDED
#define QUEUE_INCLUDED

#include <cstring>
#include <stdexcept>

namespace dsa
{
   /**
    * @brief A template circular Queue class using heap array.
    *
    * @tparam type Type of data to store in the Queue.
    * @tparam length Size of the Queue.
    */
   template<typename type, int length>
      class Queue
      {

         private:

            using Queue_t = Queue<type, length>;

            int _front; /**< Member variable to store the index of the
                          front of the Queue.*/
            int _rear;  /**< Member variable to store the index of the
                          last element of the Queue.*/
            type* _m;   /**< Pointer member to hold an allocated array
                          on the heap.*/

            /**
             * @brief Gets the front index of the circular Queue.
             * @return Front index.
             */
            inline int _getFront() const
            {
               int rem = _front % length;
               return (rem < 0) ? (rem + length) : rem;
            }

            /**
             * @brief Gets the rear index of the circular Queue.
             * @return Rear index.
             */
            inline int _getRear() const
            {
               int rem = _rear % length;
               return (rem < 0) ? (rem + length) : rem;
            }

         public:

            /**
             * @brief Constructor
             */
            Queue(): _front(-1), _rear(-1), _m(new type[length]){}

            /**
             * @brief Copy Constructor.
             *
             * @param aQueue A Queue object.
             *
             * Creates a deep copy of the given Queue.
             */
            Queue(const Queue_t& aQueue): _front(aQueue._front),
            _rear(aQueue._rear), _m(nullptr)
         {
            _m = new type[length]();
            std::memcpy(_m, aQueue._m, sizeof(type) * length);
         }

            /**
             * @brief Move Constructor
             *
             * @param aQueue Move reference to a Queue object.
             */
            Queue(Queue_t&& aQueue): _front(aQueue._front), _rear(aQueue._rear),
            _m(aQueue._m)
         {
            aQueue._front = -1;
            aQueue._rear = -1;
            aQueue._m = nullptr;
         }

            /**
             * @brief Destructor.
             */
            ~Queue()
            {
               delete[] _m;
            }

            /**
             * @brief Copy assignment operator overload.
             *
             * @param aQueue A Queue object.
             * @return Reference to a Queue object.
             */
            Queue_t& operator=(const Queue_t& aQueue)
            {
               if (this != &aQueue)
               {
                  _front = aQueue._front;
                  _rear = aQueue._rear;
                  std::memcpy(_m, aQueue._m, sizeof(type) * length);
               }
               return *this;
            }

            /**
             * @brief Move assignment operator overload.
             *
             * @param aQueue Move reference to a Queue object.
             * @return Reference to a Queue object.
             */
            Queue_t& operator=(const Queue_t&& aQueue)
            {
               if(this != &aQueue)
               {
                  delete[] _m;
                  _front = aQueue._front;
                  _rear = aQueue._rear;
                  _m = aQueue._m;

                  aQueue._front = -1;
                  aQueue._rear = -1;
                  aQueue._m = nullptr;
               }
               return *this;
            }

            /**
             * @brief Checks if the given Queue is empty.
             *
             * @return True if empty, Else false.
             */
            inline bool isEmpty() const { return _getFront() == _getRear(); }

            /**
             * @brief Returns the front element of the given Queue.
             *
             * @return Element at the front of the Queue.
             */
            type front() const
            {
               if(isEmpty())
               {
                  throw std::out_of_range("empty Queue!!");
               }
               else
               {
                  return _m[_front + 1];
               }
            }

            /**
             * @brief Inserts an element to the given Queue.
             *
             * @param item Element to insert into the given Queue.
             */
            void enqueue(const type& item)
            {
               if(isEmpty() || (_getFront() == (_rear + 2) % length))
               {
                  _rear = (_rear + 1) % length;
                  _m[_rear] = item;
               }
               else
               {
                  throw std::out_of_range("queue full! Cannot enqueue!!");
               }
            }

            /**
             * @brief Removes an element from the given Queue.
             */
            void dequeue()
            {
               if(!isEmpty())
               {
                  _front = (_front + 1) % length;
                  _m[_front] = static_cast<type>(NULL);
               }
               else
               {
                  throw std::out_of_range("queue empty! Cannot dequeue!!");
               }
            }
      };
}

#endif
