/*
 * Copyright 2020 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file Array.hpp
 * @author Avinash Chikkadlur Ravi Shanakar
 * @brief Header file for templated array class.
 */

#ifndef ARRAY_INCLUDED
#define ARRAY_INCLUDED

#include <cstring>
#include <stdexcept>

namespace dsa
{
   /**
    * @brief A template Array class using heap memory allocation.
    *
    * @tparam type Type of data to store in the Array.
    * @tparam length  Size of the Array.
    */
   template<typename type, int length>
      class Array
      {

         private:

            using Array_t = Array<type, length>;

         protected:

            type* _m;          /**< Member pointer to hold the address of the
                                 allocated memory */

         public:

            /**
             * @brief Iterator class.
             */
            class Iterator
            {
               private:

                  type* _currentElement; /**< Pointer to the current array
                                           element. */

               public:

                  /**
                   * @brief Constructor
                   *
                   * @param aPointer A pointer to initialize the iterator.
                   */
                  Iterator(type* aPointer=nullptr): _currentElement(aPointer){}

                  /**
                   * @brief Copy constructor.
                   *
                   * @param anIterator Reference to an Array iterator.
                   */
                  Iterator(const Iterator& anIterator):
                     _currentElement(anIterator._currentElement){}

                  /**
                   * @brief Move constructor.
                   *
                   * @param anIterator Move reference to an Array iterator.
                   */
                  Iterator(Iterator&& anIterator):
                     _currentElement(anIterator._currentElement)
               {
                  anIterator._currentElement = nullptr;
               }

                  /**
                   * @brief Copy assignment operation.
                   *
                   * @param anIterator Reference to an Array iterator.
                   * @return An Iterator.
                   */
                  Iterator& operator=(const Iterator& anIterator)
                  {
                     if(this != &anIterator)
                     {
                        _currentElement = anIterator._currentElement;
                     }
                     return *this;
                  }

                  /**
                   * @brief Move assignment operation.
                   *
                   * @param anIterator Reference to an Array iterator.
                   * @return An Iterator.
                   */
                  Iterator& operator=(Iterator&& anIterator)
                  {
                     if(this != &anIterator)
                     {
                        _currentElement = anIterator._currentElement;
                        anIterator._currentElement = nullptr;
                     }
                     return *this;
                  }

                  /**
                   * @brief Binary + operator overload.
                   *
                   * @param value Amount by which the value is incremented.
                   * @return An iterator incremented by the given value.
                   */
                  inline Iterator operator+(int value)
                  {
                     return Iterator(_currentElement + value);
                  }

                  /**
                   * @brief Binary - operator overload.
                   *
                   * @param value Amount by which the value is decremented.
                   * @return An iterator decremented by the given value.
                   */
                  inline Iterator operator-(int value)
                  {
                     return Iterator(_currentElement - value);
                  }

                  /**
                   * @brief Binary += operator overload.
                   *
                   * @param value Amount by which the iterator is incremented.
                   */
                  inline Iterator& operator+=(int value)
                  {
                     _currentElement = _currentElement + value;
                     return *this;
                  }

                  /**
                   * @brief Binary -= operator overload.
                   *
                   * @param value Amount by which the iterator is decremented.
                   * @return Iterator
                   */
                  inline Iterator& operator-=(int value)
                  {
                     _currentElement = _currentElement - value;
                     return *this;
                  }

                  /**
                   * @brief Unary prefix ++ operator overload.
                   *
                   * @return Iterator
                   */
                  inline Iterator& operator++()
                  {
                     _currentElement = _currentElement + 1;
                     return *this;
                  }

                  /**
                   * @brief Unary prefix -- operator overload.
                   *
                   * @return Iterator
                   */
                  inline Iterator& operator--()
                  {
                     _currentElement = _currentElement - 1;
                     return *this;
                  }

                  /**
                   * @brief Unary postfix ++ operator overload.
                   *
                   * @return Iterator
                   */
                  inline Iterator operator++(int)
                  {
                     Iterator old{*this};
                     operator++();
                     return old;
                  }

                  /**
                   * @brief Unary postfix -- operator overload.
                   *
                   * @return Iterator
                   */
                  inline Iterator operator--(int)
                  {
                     Iterator old{*this};
                     operator--();
                     return old;
                  }

                  /**
                   * @brief Dereference operator overload.
                   *
                   * @return Value at the dereferenced element.
                   */
                  inline type& operator*() const { return *_currentElement; }

                  /**
                   * @brief Binary == operator overload
                   *
                   * @param anIterator Reference to an iterator.
                   * @return True if both iterators point to the same element.
                   * Else, false.
                   */
                  inline bool operator==(const Iterator& anIterator)
                  {
                     return _currentElement == anIterator._currentElement;
                  }

                  /**
                   * @brief Binary != operator overload
                   *
                   * @param anIterator Reference to an iterator.
                   * @return True if both iterators point to different elements.
                   * Else, false.
                   */
                  inline bool operator!=(const Iterator& anIterator)
                  {
                     return _currentElement != anIterator._currentElement;
                  }
            };

            /**
             * @brief Variadic template constructor.
             *
             * @tparam T Variadic template type @param args List of values to
             * initialize the array container.
             *
             * Constructs an array of given type and length and initialize by
             * expanding the variadic arguments pack.
             *
             * @remark If the number of elements in the arguments pack is less
             * than the given length of the array, the remainder elements are
             * initialized to zero.
             */
            template<typename... T>
               Array(const T&... args): _m(new type[length]{args...}){}

            /**
             * @brief Copy constructor.
             *
             * @param anArray Reference to an Array object.             *
             *
             * Constructs a true copy of the given array by allocating heap
             * memory.
             */
            Array(const Array_t& anArray): _m(nullptr)
         {
            type* temp = new type[length]();
            std::memcpy(temp, anArray._m, sizeof(type) * length);
            _m = temp;
         }

            /**
             * @brief Move constructor.
             *
             * @param anArray Move reference to an Array object.
             */
            Array(Array_t&& anArray): _m(anArray._m)
         {
            anArray._m = nullptr;
         }

            /**
             * @brief Copy assignment operation
             *
             * @param anArray Copy reference to an Array instance.
             * @return An Array object.
             */
            Array_t& operator=(const Array_t& anArray)
            {
               if(this != &anArray)
               {
                  std::memcpy(_m, anArray._m, sizeof(type) * length);
               }
               return *this;
            }

            /**
             * @brief Move assignment operation.
             *
             * @param anArray Move referene to an Array instance.
             * @return An Array object.
             */
            Array_t& operator=(Array_t&& anArray)
            {
               if(this != &anArray)
               {
                  delete[] _m;
                  _m = anArray._m;

                  anArray._m = nullptr;
               }
               return *this;
            }

            /**
             * @brief Destructor
             */
            ~Array()
            {
               delete[] _m;
            }

            /**
             * @brief Subscript operator overload.
             *
             * @param index Array index.
             * @return A reference to the value at the given index.
             */
            inline type& operator[](const int& index)const
            {
               if(index >= length)
               {
                  throw std::out_of_range("index out of range!!");
               }
               else { return _m[index]; }
            }

            /**
             * Returns the size of the array.
             *
             * @return Size of the array.
             */
            constexpr inline int size() { return length; }

            /**
             * @brief Returns an Iterator to the start of the array.
             *
             * @return Iterator to the start of the array.
             */
            inline Iterator begin() { return Iterator(_m); }

            /**
             * @brief Returns an Iterator to the end of the array.
             *
             * @return Iterator to the end of the array.
             */
            inline Iterator end() { return Iterator(_m + length); }

            /**
             * @brief Returns an iterator to the start of the array in reverse.
             *
             * @return Iterator to the start of the array in reverse.
             */
            inline Iterator rBegin() { return Iterator(_m + (length - 1)); }

            /**
             * @brief Returns an iterator to the end of the array in reverse.
             *
             * @return Iterator to the end of the array in reverse.
             */
            inline Iterator rEnd() { return Iterator(_m - 1); }
      };
}

#endif
