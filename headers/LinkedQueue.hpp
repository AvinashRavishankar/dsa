/*
 * Copyright 2020 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file LinkedQueue.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for linked-list based templated stack.
 */

#ifndef LINKEDQUEUE_INCLUDED
#define LINKEDQUEUE_INCLUDED

#include <utility>
#include "SingleList.hpp"

namespace dsa
{
   /**
    * @brief A template queue class constructed with single-linked list.
    *
    * @tparam type Type of data to store in the Queue.
    */
   template<typename type>
      class LinkedQueue: private SingleList<type>
      {
         private:

            using Node_t  = Node<type>;
            using List_t  = SingleList<type>;
            using Queue_t = LinkedQueue<type>;

         public:

            /**
             * @brief Default constructor.
             */
            LinkedQueue(): List_t(){}

            /**
             * @brief Copy constructor.
             *
             * @param aQueue A LinkedQueue object.
             */
            LinkedQueue(const Queue_t& aQueue): List_t(aQueue){}

            /**
             * @brief Move constructor.
             *
             * @param aQueue Move reference to a LinkedStack object.
             */
            LinkedQueue(Queue_t&& aQueue): List_t(std::move(aQueue)){}

            /**
             * @brief Destructor.
             */
            ~LinkedQueue(){}

            /**
             * @brief Copy assignment operator overload.
             *
             * @param aQueue A LinkedQueue object.
             */
            Queue_t& operator=(const Queue_t& aQueue)
            {
               if(this != &aQueue)
               {
                  List_t::operator=(aQueue);
               }
               return *this;
            }

            /**
             * @brief Move assignment operator overload.
             *
             * @param aQueue Move reference to a LinkedQueue object.
             */
            Queue_t& operator=(Queue_t&& aQueue)
            {
               if(this != &aQueue)
               {
                  List_t::operator=(std::move(aQueue));
               }
               return *this;
            }

            /**
             * @brief Checks if the given queue is empty.
             *
             * @return True if empty. Else, false.
             */
            bool isEmpty() const { return List_t::isEmpty(); }

            /**
             * @brief Fetches the element at the front of the queue.
             *
             * @return Front element of the queue.
             */
            type front() const
            {
               if(isEmpty())
               {
                  throw std::logic_error("empty Queue!!");
               }
               else
               {
                  return List_t::operator[](0);
               }
            }

            /**
             * @brief Inserts data at the end of the queue.
             *
             * @param aData Data to insert into the queue.
             * @remark Enqueue is an O(n) complex operation.
             */
            void enqueue(const type& aData)
            {
               List_t::insert(aData);
            }

            /**
             * @brief Deletes data at the front of the queue.
             */
            void dequeue()
            {
               try
               {
                  List_t::removeAt(0);
               }
               catch(std::logic_error& e)
               {
                  throw std::logic_error("empty Queue!!");
               }
            }
      };
}

#endif
