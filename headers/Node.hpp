/*
 * Copyright 2020 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file Node.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated node class with single link.
 */

#ifndef NODE_INCLUDED
#define NODE_INCLUDED

namespace dsa
{
#ifdef SINGLELIST_INCLUDED
   template<typename type> class SingleList;
#endif

   /**
    * @brief A template Node class.
    *
    * @tparam type Type of data to store in the Node.
    */
   template<typename type>
      class Node
      {
         private:

            using Node_t = Node<type>;

            type _d;    /**< Member variable to hold data in the Node.*/
            Node_t* _l; /**< Pointer member to hold the address of the link
                          node.*/

            /**
             * @brief Fetches the linked Node.
             *
             * @return address of the linked Node.
             */
            inline Node_t* _node() { return _l; }

            /**
             * @brief Set the address of the linked Node.
             *
             * @param aLink Address of the Node to be liked.
             */
            inline void _setNode(Node_t* aLink) { _l = aLink; }

#ifdef SINGLELIST_INCLUDED
            friend SingleList<type>;
#endif

         protected:

            /**
             * @brief constructor.
             *
             * @param data Reference to the data to store in the Node.
             * @param link Adress of a Node object.
             */
            Node(const type& data = static_cast<type>(NULL),
                  Node_t* link = nullptr): _d(data), _l(link){}

            /**
             * @brief Copy constructor.
             *
             * @param aNode A Node object.
             */
            Node(const Node_t& aNode): _d(aNode._d), _l(aNode._l){}

            /**
             * @brief Move constructor.
             *
             * @param aNode Move Reference to a Node object.
             */
            Node(Node_t&& aNode): _d(aNode._d), _l(aNode._l)
         {
            aNode._d = static_cast<type>(NULL);
            aNode._l = nullptr;
         }

            /**
             * @brief Destructor.
             */
            virtual ~Node() { if(_l) { _l = nullptr; } }

            /**
             * @brief Copy assignment operator.
             *
             * @param aNode A Node object.
             */
            Node_t& operator=(const Node_t& aNode)
            {
               if(this != &aNode)
               {
                  _d = aNode._d;
                  _l = aNode._l;
               }
               return *this;
            }

            /**
             * @brief Move assignment operator.
             *
             * @param aNode Move reference to a Node object.
             */
            Node_t& operator=(Node_t&& aNode)
            {
               if(this != &aNode)
               {
                  if(_l) { delete _l; }

                  _d = aNode._d;
                  _l = aNode._l;

                  aNode._d = static_cast<type>(NULL);
                  aNode._l = nullptr;
               }
               return *this;
            }

         public:

            /**
             * @brief Fetches the data stored in the Node.
             *
             * @return Data stored in the Node.
             */
            inline type& data() { return _d; }

            /**
             * @brief Fetches the address of the linked Node.
             *
             * @return Address of the linked Node.
             */
            inline const Node_t* node()const { return _l; }
      };
}

#endif
