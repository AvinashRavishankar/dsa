/*
 * Copyright 2020 Avinash Chikkadlur Ravi Shankar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file DoubleNode.hpp
 * @author Avinash Chikkadlur Ravi Shankar
 * @brief Header file for templated node class with double link.
 */

#ifndef DOUBLENODE_INCLUDED
#define DOUBLENODE_INCLUDED

namespace dsa
{
#ifdef DOUBLELIST_INCLUDED
   template<typename type> class DoubleList;
#endif

   /**
    * @brief A Template class for double-linked node.
    *
    * @tparam type Type of data to store in the node.
    */
   template<typename type>
      class DoubleNode
      {
         private:
            using Node_t = DoubleNode<type>;

            type _d;     /**< Member variable to hold data in the Node.*/
            Node_t* _ll; /**< Pointer member to hold the address of the left
                           link Node.*/
            Node_t* _rl; /**< Pointer member to hold the address of the right
                           link Node*/

#ifdef DOUBLELIST_INCLUDED
            friend DoubleList<type>;
#endif

         protected:

            /**
             * @brief Fetches the node linked to the left.
             *
             * @return Address of the left node.
             */
            inline Node_t* _leftNode() {return _ll;}

            /**
             * @brief Fetches the Node linked to the right.
             *
             * @return Address of the right node.
             */
            inline Node_t* _rightNode() {return _rl;}

            /**
             * @brief Sets the given node as left link.
             *
             * @param aLink Address of the Node to be linked to the left.
             */
            inline void _setLeftNode(Node_t* aLink) { _ll = aLink; }

            /**
             * @brief Sets the given node as right link.
             *
             * @param aLink Address of the Node to be linked to the right.
             */
            inline void _setRightNode(Node_t* aLink) { _rl = aLink; }

            /**
             * @brief Constructor
             *
             * @param data Reference to the data to store in the node.
             * @param llink Address of the node to link as the left child.
             * @param rlink Address of the node to link as the right child.
             */
            DoubleNode(const type& data = static_cast<type>(NULL),
                  Node_t* llink = nullptr, Node_t* rlink = nullptr):
               _d(data), _ll(llink), _rl(rlink){}

            /**
             * @brief Copy constructor.
             *
             * @param aNode A DoubleNode object.
             */
            DoubleNode(const Node_t& aNode):
               _d(aNode._d), _ll(aNode._ll), _rl(aNode._rl){}

            /**
             * @brief Move constructor.
             *
             * @param aNode A DoubleNode object.
             */
            DoubleNode(Node_t&& aNode):
               _d(aNode._d), _ll(aNode._ll), _rl(aNode._rl)
         {
            aNode._d = static_cast<type>(NULL);
            aNode._ll = nullptr;
            aNode._rl = nullptr;
         }

            /**
             * @brief Destructor.
             */
            virtual ~DoubleNode()
            {
               if(_rl)
               {
                  _rl->_setLeftNode(nullptr);
                  _rl = nullptr;
               }

               if(_ll)
               {
                  _ll->_setRightNode(nullptr);
                  _ll = nullptr;
               }
            }

            /**
             * @brief Copy assignment operator overload.
             *
             * @param aNode A DoubleNode object.
             */
            Node_t& operator=(const Node_t& aNode)
            {
               if(this != &aNode)
               {
                  _d = aNode._d;
                  _ll = aNode._ll;
                  _rl = aNode._rl;
               }
               return *this;
            }

            /**
             * @brief Move assignment operator overload.
             *
             * @param aNode Move reference to a DoubleNode object.
             */
            Node_t& operator=(Node_t&& aNode)
            {
               if(this != &aNode)
               {
                  _d = aNode._d;
                  _ll = aNode._ll;
                  _rl = aNode._rl;

                  aNode._d = static_cast<type>(NULL);
                  aNode._ll = nullptr;
                  aNode._rl = nullptr;
               }
               return *this;
            }

         public:

            /**
             * @brief Fetches the data stored in the node.
             *
             * @return Data stored in the node.
             */
            inline type& data(){ return _d; }

            /**
             * @brief Fetches the address of the left linked node.
             *
             * @return Address of the left linked node.
             */
            inline const Node_t* leftNode()const { return _ll; }

            /**
             * @brief Fetches the address of the right linked node.
             *
             * @return Address of the right linked node.
             */
            inline const Node_t* rightNode()const { return _rl; }
      };

}

#endif
