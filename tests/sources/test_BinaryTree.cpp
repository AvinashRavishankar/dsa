#include "test_BinaryTree.hpp"

BEGIN_TEST(BinaryTree, Empty)
{
   dsa::BinaryTree<int> bTree;
   EXPECT_EQ(bTree.isEmpty(), true);
}END_TEST

BEGIN_TEST(BinaryTree, Insert)
{
   dsa::BinaryTree<int> bTree1, bTree2, bTree3, bTree4;

   bTree1 = dsa::BinaryTree<int>::insert(5);
   bTree2 = dsa::BinaryTree<int>::insert(15);
   bTree3 = dsa::BinaryTree<int>::insert(10, &bTree1, &bTree2);
   bTree4 = dsa::BinaryTree<int>::insert(20, &bTree3);
   bTree4 = dsa::BinaryTree<int>::insert(25, nullptr, &bTree4);

   dsa::DoubleList<int> actual = bTree4.traversePreOrder();
   dsa::DoubleList<int> expected;
   expected.insert(25);
   expected.insert(20);
   expected.insert(10);
   expected.insert(5);
   expected.insert(15);

   for (int i{0}; i < expected.size(); ++i)
   {
      EXPECT_EQ(actual[i], expected[i])
   }

   EXPECT_EQ(bTree1.isEmpty(), true)
   EXPECT_EQ(bTree2.isEmpty(), true)
   EXPECT_EQ(bTree3.isEmpty(), true)
}END_TEST

BEGIN_TEST(BinaryTree, TraversePreOrder)
{
   dsa::BinaryTree<int> bTree1, bTree2, bTree3, bTree4;

   bTree1 = dsa::BinaryTree<int>::insert(5);
   bTree2 = dsa::BinaryTree<int>::insert(15);
   bTree3 = dsa::BinaryTree<int>::insert(10, &bTree1, &bTree2);
   bTree4 = dsa::BinaryTree<int>::insert(20, &bTree3);
   bTree4 = dsa::BinaryTree<int>::insert(25, nullptr, &bTree4);

   dsa::DoubleList<int> actual = bTree4.traversePreOrder();
   dsa::DoubleList<int> expected;
   expected.insert(25);
   expected.insert(20);
   expected.insert(10);
   expected.insert(5);
   expected.insert(15);

   for (int i{0}; i < expected.size(); ++i)
   {
      EXPECT_EQ(actual[i], expected[i])
   }
}END_TEST

BEGIN_TEST(BinaryTree, TraverseInOrder)
{
   dsa::BinaryTree<int> bTree1, bTree2, bTree3, bTree4;

   bTree1 = dsa::BinaryTree<int>::insert(5);
   bTree2 = dsa::BinaryTree<int>::insert(15);
   bTree3 = dsa::BinaryTree<int>::insert(10, &bTree1, &bTree2);
   bTree4 = dsa::BinaryTree<int>::insert(20, &bTree3);
   bTree4 = dsa::BinaryTree<int>::insert(25, nullptr, &bTree4);

   dsa::DoubleList<int> actual = bTree4.traverseInOrder();
   dsa::DoubleList<int> expected;
   expected.insert(25);
   expected.insert(5);
   expected.insert(10);
   expected.insert(15);
   expected.insert(20);

   for (int i{0}; i < expected.size(); ++i)
   {
      EXPECT_EQ(actual[i], expected[i])
   }
}END_TEST

BEGIN_TEST(BinaryTree, TraversePostOrder)
{
   dsa::BinaryTree<int> bTree1, bTree2, bTree3, bTree4;

   bTree1 = dsa::BinaryTree<int>::insert(5);
   bTree2 = dsa::BinaryTree<int>::insert(15);
   bTree3 = dsa::BinaryTree<int>::insert(10, &bTree1, &bTree2);
   bTree4 = dsa::BinaryTree<int>::insert(20, &bTree3);
   bTree4 = dsa::BinaryTree<int>::insert(25, nullptr, &bTree4);

   dsa::DoubleList<int> actual = bTree4.traversePostOrder();
   dsa::DoubleList<int> expected;
   expected.insert(5);
   expected.insert(15);
   expected.insert(10);
   expected.insert(20);
   expected.insert(25);

   for (int i{0}; i < expected.size(); ++i)
   {
      EXPECT_EQ(actual[i], expected[i])
   }
}END_TEST

BEGIN_TEST(BinaryTree, AreEqual)
{
   dsa::BinaryTree<int> bTree1, bTree2, bTree3, bTree4, bTree5;

   bTree1 = dsa::BinaryTree<int>::insert(5);
   bTree2 = dsa::BinaryTree<int>::insert(15);
   bTree3 = dsa::BinaryTree<int>::insert(10, &bTree1, &bTree2);
   bTree4 = dsa::BinaryTree<int>::insert(20, &bTree3);
   bTree4 = dsa::BinaryTree<int>::insert(25, nullptr, &bTree4);

   bTree5 = bTree4;
   EXPECT_EQ(dsa::BinaryTree<int>::areEqual(bTree5, bTree4), true)
}END_TEST

BEGIN_TEST(BinaryTree, AreNotEqual)
{
   dsa::BinaryTree<int> bTree1, bTree2, bTree3, bTree4, bTree5;

   bTree1 = dsa::BinaryTree<int>::insert(5);
   bTree2 = dsa::BinaryTree<int>::insert(15);
   bTree3 = dsa::BinaryTree<int>::insert(10, &bTree1, &bTree2);
   bTree4 = dsa::BinaryTree<int>::insert(20, &bTree3);
   bTree4 = dsa::BinaryTree<int>::insert(25, nullptr, &bTree4);

   bTree1 = dsa::BinaryTree<int>::insert(5);
   bTree2 = dsa::BinaryTree<int>::insert(15);
   bTree3 = dsa::BinaryTree<int>::insert(10, &bTree1, &bTree2);
   bTree5 = dsa::BinaryTree<int>::insert(20, &bTree3);
   bTree5 = dsa::BinaryTree<int>::insert(25, &bTree5);

   EXPECT_EQ(dsa::BinaryTree<int>::areEqual(bTree5, bTree4), false)
}END_TEST
