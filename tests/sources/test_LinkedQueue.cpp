#include "test_LinkedQueue.hpp"

BEGIN_TEST(LinkedQueue, EmptyQueue)
{
   dsa::LinkedQueue<int> queue;
   EXPECT_EQ(queue.isEmpty(), true)
}END_TEST

BEGIN_TEST(LinkedQueue, FrontEmptyQueue)
{
   dsa::LinkedQueue<int> queue;
   try
   {
      queue.front();
      EXPECT_EQ(true, false)
   }
   catch(const std::logic_error& e)
   {
      EXPECT_EQ(true, true)
   }
}END_TEST

BEGIN_TEST(LinkedQueue, FrontNonEmptyQueue)
{
   dsa::LinkedQueue<int> queue;
   queue.enqueue(2);
   EXPECT_EQ(queue.front(), 2)
}END_TEST

BEGIN_TEST(LinkedQueue, Enqueue)
{
   dsa::LinkedQueue<int> queue;
   queue.enqueue(2);
   EXPECT_EQ(queue.front(), 2)
   EXPECT_EQ(queue.isEmpty(), false)
}END_TEST

BEGIN_TEST(LinkedQueue, Dequeue)
{
   dsa::LinkedQueue<int> queue;
   queue.enqueue(2);
   queue.enqueue(4);

   EXPECT_EQ(queue.front(), 2)
   queue.dequeue();
   EXPECT_EQ(queue.front(), 4)
}END_TEST

BEGIN_TEST(LinkedQueue, DequeueEmptyQueue)
{
   dsa::LinkedQueue<int> queue;
   try
   {
      queue.dequeue();
      EXPECT_EQ(true, false)
   }
   catch(const std::logic_error& e)
   {
      EXPECT_EQ(true, true)
   }
}END_TEST

BEGIN_TEST(LinkedQueue, CopyAssignment)
{
   dsa::LinkedQueue<int> queue1;
   queue1.enqueue(2);
   queue1.enqueue(4);
   dsa::LinkedQueue<int> queue2;
   queue2.enqueue(3);
   queue2.enqueue(6);

   queue1 = queue2;
   EXPECT_EQ(queue1.front(), queue2.front())
   queue1.dequeue();
   queue2.dequeue();
   EXPECT_EQ(queue1.front(), queue2.front())
}END_TEST

BEGIN_TEST(LinkedQueue, MoveAssignment)
{
   dsa::LinkedQueue<int> queue1;
   queue1.enqueue(2);
   queue1.enqueue(4);
   dsa::LinkedQueue<int> queue2;
   queue2.enqueue(3);
   queue2.enqueue(6);

   queue1 = std::move(queue2);
   EXPECT_EQ(queue1.front(), 3)
   queue1.dequeue();
   EXPECT_EQ(queue1.front(), 6)
   EXPECT_EQ(queue2.isEmpty(), true)
}END_TEST
