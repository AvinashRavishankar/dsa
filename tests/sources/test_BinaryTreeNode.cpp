#include "test_BinaryTreeNode.hpp"

BEGIN_TEST(BinaryTreeNode, Data)
{
   dsa::BinaryTreeNode<int> node1;
   EXPECT_EQ(node1.data(), static_cast<int>(NULL))

   dsa::BinaryTreeNode<int> node2(10);
   EXPECT_EQ(node2.data(), 10)
}END_TEST

BEGIN_TEST(BinaryTreeNode, LeftNode)
{
   dsa::BinaryTreeNode<int> node1;
   EXPECT_EQ(node1.leftNode(), nullptr)

   dsa::BinaryTreeNode<int> node2(0, &node1);
   EXPECT_EQ(node2.leftNode(), &node1)
}END_TEST

BEGIN_TEST(BinaryTreeNode, RightNode)
{
   dsa::BinaryTreeNode<int> node1;
   EXPECT_EQ(node1.rightNode(), nullptr)

   dsa::BinaryTreeNode<int> node2(0, nullptr, &node1);
   EXPECT_EQ(node2.rightNode(), &node1)
}END_TEST

BEGIN_TEST(BinaryTreeNode, CopyAssignment)
{
   dsa::BinaryTreeNode<int> node1(5);
   dsa::BinaryTreeNode<int> node2(10, &node1);
   dsa::BinaryTreeNode<int> node3(15, nullptr, &node1);

   node2 = node3;
   EXPECT_EQ(node2.data(), node3.data())
   EXPECT_EQ(node2.leftNode(), node3.leftNode())
   EXPECT_EQ(node2.rightNode(), node3.rightNode())
}END_TEST

BEGIN_TEST(BinaryTreeNode, MoveAssignment)
{
   dsa::BinaryTreeNode<int> node1(5);
   dsa::BinaryTreeNode<int> node2(10, &node1);
   dsa::BinaryTreeNode<int> node3(15, nullptr, &node1);

   node2 = std::move(node3);
   EXPECT_EQ(node2.data(), 15)
   EXPECT_EQ(node2.leftNode(), nullptr)
   EXPECT_EQ(node2.rightNode(), &node1)
   EXPECT_EQ(node3.data(), static_cast<int>(NULL))
   EXPECT_EQ(node3.leftNode(), nullptr)
   EXPECT_EQ(node3.rightNode(), nullptr)
}END_TEST
