#include "test_LinkedStack.hpp"

BEGIN_TEST(LinkedStack, EmptyStack)
{
   dsa::LinkedStack<int> stack;
   EXPECT_EQ(stack.isEmpty(), true)
}END_TEST

BEGIN_TEST(LinkedStack, TopEmptyStack)
{
   dsa::LinkedStack<int> stack;
   try
   {
      stack.top();
      EXPECT_EQ(true, false)
   }
   catch(const std::logic_error& e)
   {
      EXPECT_EQ(true, true)
   }
}END_TEST

BEGIN_TEST(LinkedStack, TopNonEmptyStack)
{
   dsa::LinkedStack<int> stack;
   stack.push(2);
   try
   {
      EXPECT_EQ(stack.top(), 2)
   }
   catch(const std::logic_error& e)
   {
      EXPECT_EQ(true, false)
   }
}END_TEST

BEGIN_TEST(LinkedStack, Push)
{
   dsa::LinkedStack<int> stack;
   stack.push(2);
   EXPECT_EQ(stack.top(), 2)
}END_TEST

BEGIN_TEST(LinkedStack, Pop)
{
   dsa::LinkedStack<int> stack;
   stack.push(2);
   stack.push(4);

   EXPECT_EQ(stack.top(), 4)
   stack.pop();
   EXPECT_EQ(stack.top(), 2)
}END_TEST

BEGIN_TEST(LinkedStack, PopEmptyStack)
{
   dsa::LinkedStack<int> stack;
   try
   {
      stack.pop();
      EXPECT_EQ(true, false)
   }
   catch(const std::logic_error& e)
   {
      EXPECT_EQ(true, true)
   }
}END_TEST

BEGIN_TEST(LinkedStack, CopyAssignment)
{
   dsa::LinkedStack<int> stack1;
   stack1.push(2);
   stack1.push(4);

   dsa::LinkedStack<int> stack2;
   stack2.push(3);
   stack2.push(6);

   stack1 = stack2;
   EXPECT_EQ(stack1.top(), stack2.top())
   stack1.pop();
   stack2.pop();
   EXPECT_EQ(stack1.top(), stack2.top())
}END_TEST

BEGIN_TEST(LinkedStack, MoveAssignment)
{
   dsa::LinkedStack<int> stack1;
   stack1.push(2);
   stack1.push(4);

   dsa::LinkedStack<int> stack2;
   stack2.push(3);
   stack2.push(6);

   stack1 = std::move(stack2);
   EXPECT_EQ(stack1.top(), 6)
   stack1.pop();
   EXPECT_EQ(stack1.top(), 3)
   EXPECT_EQ(stack2.isEmpty(), true)
}END_TEST
