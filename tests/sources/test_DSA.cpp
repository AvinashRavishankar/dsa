#include "test_Array.hpp"
#include "test_Stack.hpp"
#include "test_Queue.hpp"
#include "test_SingleList.hpp"
#include "test_LinkedStack.hpp"
#include "test_LinkedQueue.hpp"
#include "test_DoubleList.hpp"
#include "test_BinaryTree.hpp"
#include "test_BinarySearchTree.hpp"
#include <sanitizer/lsan_interface.h>

INIT_TEST_SUMMARY

int main(){

   RUN_TEST(Array, CopyAssignment)
   RUN_TEST(Array, MoveAssignment)
   RUN_TEST(Array, SequentialSearchFound)
   RUN_TEST(Array, SequentialSearchNotFound)
   RUN_TEST(Array, BinarySearchFoundOdd)
   RUN_TEST(Array, BinarySearchFoundEven)
   RUN_TEST(Array, BinarySearchNotFoundOdd)
   RUN_TEST(Array, BinarySearchNotFoundEven)
   RUN_TEST(Array, FibonacciSearchFoundLHS)
   RUN_TEST(Array, FibonacciSearchFoundRHS)
   RUN_TEST(Array, FibonacciSearchNotFound)
   RUN_TEST(Array, InsertionSort)
   RUN_TEST(Array, QuickSort)
   RUN_TEST(Array, MergeSort)
   RUN_TEST(Array, HeapSort)
   RUN_TEST(Array, RadixSort_1)
   RUN_TEST(Array, RadixSort_2)

   RUN_TEST(Stack, EmptyStack)
   RUN_TEST(Stack, TopEmptyStack)
   RUN_TEST(Stack, TopNonEmptyStack)
   RUN_TEST(Stack, Push)
   RUN_TEST(Stack, Pop)
   RUN_TEST(Stack, Overflow)
   RUN_TEST(Stack, PopEmptyStack)

   RUN_TEST(Queue, EmptyQueue)
   RUN_TEST(Queue, FrontEmptyQueue)
   RUN_TEST(Queue, FrontNonEmptyQueue)
   RUN_TEST(Queue, Enqueue)
   RUN_TEST(Queue, Dequeue)
   RUN_TEST(Queue, Overflow)
   RUN_TEST(Queue, DequeueEmptyQueue)

   RUN_TEST(SingleList, Empty)
   RUN_TEST(SingleList, Insert)
   RUN_TEST(SingleList, InsertAtPosition)
   RUN_TEST(SingleList, Remove)
   RUN_TEST(SingleList, RemoveAtPosition)
   RUN_TEST(SingleList, CopyAssignment)
   RUN_TEST(SingleList, MoveAssignment)
   RUN_TEST(SingleList, SequentialSearchFound)
   RUN_TEST(SingleList, SequentialSearchNotFound)
   RUN_TEST(SingleList, BinarySearchFoundOdd)
   RUN_TEST(SingleList, BinarySearchFoundEven)
   RUN_TEST(SingleList, BinarySearchNotFoundOdd)
   RUN_TEST(SingleList, BinarySearchNotFoundEven)
   RUN_TEST(SingleList, FibonacciSearchFoundLHS)
   RUN_TEST(SingleList, FibonacciSearchFoundRHS)
   RUN_TEST(SingleList, FibonacciSearchNotFound)
   RUN_TEST(SingleList, MergeSort)
   RUN_TEST(SingleList, HeapSort)
   RUN_TEST(SingleList, RadixSort)

   RUN_TEST(LinkedStack, EmptyStack)
   RUN_TEST(LinkedStack, TopEmptyStack)
   RUN_TEST(LinkedStack, TopNonEmptyStack)
   RUN_TEST(LinkedStack, Push)
   RUN_TEST(LinkedStack, Pop)
   RUN_TEST(LinkedStack, PopEmptyStack)
   RUN_TEST(LinkedStack, CopyAssignment)
   RUN_TEST(LinkedStack, MoveAssignment)

   RUN_TEST(LinkedQueue, EmptyQueue)
   RUN_TEST(LinkedQueue, FrontEmptyQueue)
   RUN_TEST(LinkedQueue, FrontNonEmptyQueue)
   RUN_TEST(LinkedQueue, Enqueue)
   RUN_TEST(LinkedQueue, Dequeue)
   RUN_TEST(LinkedQueue, DequeueEmptyQueue)
   RUN_TEST(LinkedQueue, CopyAssignment)
   RUN_TEST(LinkedQueue, MoveAssignment)

   RUN_TEST(DoubleList, Empty)
   RUN_TEST(DoubleList, Insert)
   RUN_TEST(DoubleList, InsertAtPosition)
   RUN_TEST(DoubleList, Remove)
   RUN_TEST(DoubleList, RemoveAtPosition)
   RUN_TEST(DoubleList, CopyAssignment)
   RUN_TEST(DoubleList, MoveAssignment)
   RUN_TEST(DoubleList, SequentialSearchFound)
   RUN_TEST(DoubleList, SequentialSearchNotFound)
   RUN_TEST(DoubleList, BinarySearchFoundOdd)
   RUN_TEST(DoubleList, BinarySearchFoundEven)
   RUN_TEST(DoubleList, BinarySearchNotFoundOdd)
   RUN_TEST(DoubleList, BinarySearchNotFoundEven)
   RUN_TEST(DoubleList, FibonacciSearchFoundLHS)
   RUN_TEST(DoubleList, FibonacciSearchFoundRHS)
   RUN_TEST(DoubleList, FibonacciSearchNotFound)
   RUN_TEST(DoubleList, InsertionSort)
   RUN_TEST(DoubleList, QuickSort)
   RUN_TEST(DoubleList, MergeSort)
   RUN_TEST(DoubleList, HeapSort)
   RUN_TEST(DoubleList, RadixSort)

   RUN_TEST(BinaryTree, Empty)
   RUN_TEST(BinaryTree, Insert)
   RUN_TEST(BinaryTree, TraversePreOrder)
   RUN_TEST(BinaryTree, TraverseInOrder)
   RUN_TEST(BinaryTree, TraversePostOrder)
   RUN_TEST(BinaryTree, AreEqual)
   RUN_TEST(BinaryTree, AreNotEqual)

   RUN_TEST(BinarySearchTree, Empty)
   RUN_TEST(BinarySearchTree, Insert)
   RUN_TEST(BinarySearchTree, GetMin)
   RUN_TEST(BinarySearchTree, GetMax)
   RUN_TEST(BinarySearchTree, RemoveNode)
   RUN_TEST(BinarySearchTree, BalancedTree)
   RUN_TEST(BinarySearchTree, ImbalancedTree)

   RUN_TEST_SUMMARY

   return TEST_STATUS;
}
