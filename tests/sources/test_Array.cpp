#include "test_Array.hpp"

BEGIN_TEST(Array, CopyAssignment)
{
   dsa::Array<int, 2>array{5, 10};
   dsa::Array<int, 2>array1;

   array1 = array;
   EXPECT_EQ(array1[0], 5)
      EXPECT_EQ(array1[1], 10)
}END_TEST

BEGIN_TEST(Array, MoveAssignment)
{
   dsa::Array<int, 2>array1{5, 10};
   dsa::Array<int, 2>array2{3, 9};

   array2 = std::move(array1);
   EXPECT_EQ(array2[0], 5)
      EXPECT_EQ(array2[1], 10)
}END_TEST

BEGIN_TEST(Array, SequentialSearchFound)
{
   dsa::SequentialSearch searchSequential;
   dsa::Array<int, 5> array{1, 2, 3, 4, 5};

   int index = searchSequential(array, 3);
   EXPECT_EQ(array[index], 3)
}END_TEST

BEGIN_TEST(Array, SequentialSearchNotFound)
{
   dsa::SequentialSearch searchSequential;
   dsa::Array<int, 5> array{1, 2, 3, 4, 5};

   int index = searchSequential(array, 6);
   EXPECT_EQ(index, -1)
}END_TEST

BEGIN_TEST(Array, BinarySearchFoundOdd)
{
   dsa::BinarySearch searchBinary;
   dsa::Array<int, 6> array{2, 4, 6, 8, 12};

   int index = searchBinary(array, 2);
   EXPECT_EQ(array[index], 2)
}END_TEST

BEGIN_TEST(Array, BinarySearchFoundEven)
{
   dsa::BinarySearch searchBinary;
   dsa::Array<int, 6> array{2, 4, 6, 8, 10, 12};

   int index = searchBinary(array, 2);
   EXPECT_EQ(array[index], 2)
}END_TEST

BEGIN_TEST(Array, BinarySearchNotFoundOdd)
{
   dsa::BinarySearch searchBinary;
   dsa::Array<int, 6> array{2, 4, 6, 8, 12};

   int index = searchBinary(array, 7);
   EXPECT_EQ(index, -1)
}END_TEST

BEGIN_TEST(Array, BinarySearchNotFoundEven)
{
   dsa::BinarySearch searchBinary;
   dsa::Array<int, 6> array{2, 4, 6, 8, 10, 12};

   int index = searchBinary(array, 7);
   EXPECT_EQ(index, -1)
}END_TEST

BEGIN_TEST(Array, FibonacciSearchFoundLHS)
{
   dsa::FibonacciSearch searchFibonacci;
   dsa::Array<int, 6> array{2, 4, 6, 7, 8, 10};

   int index = searchFibonacci(array, 2);
   EXPECT_EQ(array[index], 2)
}END_TEST

BEGIN_TEST(Array, FibonacciSearchFoundRHS)
{
   dsa::FibonacciSearch searchFibonacci;
   dsa::Array<int, 6> array{2, 4, 6, 7, 8, 10};

   int index = searchFibonacci(array, 10);
   EXPECT_EQ(array[index], 10)
}END_TEST

BEGIN_TEST(Array, FibonacciSearchNotFound)
{
   dsa::FibonacciSearch searchFibonacci;
   dsa::Array<int, 6> array{2, 4, 6, 7, 8, 10};

   int index1 = searchFibonacci(array, 11);
   int index2 = searchFibonacci(array, 1);
   EXPECT_EQ(index1, -1)
      EXPECT_EQ(index2, -1)
}END_TEST

BEGIN_TEST(Array, InsertionSort)
{
   dsa::InsertionSort inSort;
   dsa::Array<int, 5> array{3, 1, 2, 5, 4};
   inSort(array);

   for(int i = 1; i <= 5; ++i)
   {
      EXPECT_EQ(array[i - 1], i)
   }
}END_TEST

BEGIN_TEST(Array, QuickSort)
{
   dsa::QuickSort qSort;
   dsa::Array<int, 5> array{3, 1, 2, 5, 4};
   qSort(array);

   for(int i = 1; i <= 5; ++i)
   {
      EXPECT_EQ(array[i - 1], i)
   }
}END_TEST

BEGIN_TEST(Array, MergeSort)
{
   dsa::MergeSort mSort;
   dsa::Array<int, 5> array{3, 1, 2, 5, 4};
   dsa::Array<int, 5> ret = mSort(array);

   for(int i = 1; i <= 5; ++i)
   {
      EXPECT_EQ(ret[i - 1], i)
   }
}END_TEST

BEGIN_TEST(Array, HeapSort)
{
   dsa::HeapSort hSort;
   dsa::Array<int, 5> array{3, 1, 2, 5, 4};
   hSort(array);

   for(int i = 1; i <= 5; ++i)
   {
      EXPECT_EQ(array[i - 1], i)
   }
}END_TEST

BEGIN_TEST(Array, RadixSort_1)
{
   dsa::RadixSort rSort;
   dsa::Array<int, 5> array{3, 1, 2, 5, 4};
   rSort(array, 1);

   for(int i = 1; i <= 5; ++i)
   {
      EXPECT_EQ(array[i - 1], i)
   }
}END_TEST

BEGIN_TEST(Array, RadixSort_2)
{
   dsa::RadixSort rSort;
   dsa::Array<int, 5> array{43, 51, 22, 15, 34};
   rSort(array, 2);

   EXPECT_EQ(array[0], 15)
   EXPECT_EQ(array[1], 22)
   EXPECT_EQ(array[2], 34)
   EXPECT_EQ(array[3], 43)
   EXPECT_EQ(array[4], 51)

}END_TEST
