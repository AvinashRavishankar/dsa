#include "test_DoubleList.hpp"

BEGIN_TEST(DoubleList, Empty)
{
   dsa::DoubleList<int> list;
   EXPECT_EQ(list.isEmpty(), true)
}END_TEST

BEGIN_TEST(DoubleList, Insert)
{
   dsa::DoubleList<int> list;
   list.insert(5);
   list.insert(10);
   list.insert(15);
   EXPECT_EQ(list[0], 5)
   EXPECT_EQ(list[1], 10)
   EXPECT_EQ(list[2], 15)
}END_TEST

BEGIN_TEST(DoubleList, InsertAtPosition)
{
   dsa::DoubleList<int> list;
   list.insertAt(0, 5);
   list.insertAt(1, 15);
   list.insertAt(1, 10);
   EXPECT_EQ(list[0], 5)
   EXPECT_EQ(list[1], 10)
   EXPECT_EQ(list[2], 15)
}END_TEST

BEGIN_TEST(DoubleList, Remove)
{
   dsa::DoubleList<int> list;
   list.insert(5);
   list.insert(10);
   list.insert(15);

   list.remove();
   try
   {
      list[2];
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(list[1], 10)
      EXPECT_EQ(list[0], 5)
   }

   list.remove();
   try
   {
      list[1];
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(list[0], 5)
   }

   list.remove();
   try
   {
      list[0];
      EXPECT_EQ(true, false)
   }
   catch(const std::logic_error& e)
   {
      EXPECT_EQ(list.isEmpty(), true)
   }
}END_TEST

BEGIN_TEST(DoubleList, RemoveAtPosition)
{
   dsa::DoubleList<int> list;
   list.insert(5);
   list.insert(10);
   list.insert(15);

   list.removeAt(0);
   try
   {
      list[2];
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(list[0], 10)
      EXPECT_EQ(list[1], 15)
   }

   list.removeAt(1);
   try
   {
      list[1];
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(list[0], 10)
   }
}END_TEST

BEGIN_TEST(DoubleList, CopyAssignment)
{
   dsa::DoubleList<int> list1;
   list1.insert(2);
   list1.insert(4);
   list1.insert(6);

   dsa::DoubleList<int> list2;
   list2.insert(3);
   list2.insert(6);
   list2.insert(9);

   list2 = list1;

   EXPECT_EQ(list2[0], list1[0])
   EXPECT_EQ(list2[1], list1[1])
   EXPECT_EQ(list2[2], list1[2])
}END_TEST

BEGIN_TEST(DoubleList, MoveAssignment)
{
   dsa::DoubleList<int> list1;
   list1.insert(2);
   list1.insert(4);
   list1.insert(6);

   dsa::DoubleList<int> list2;
   list2.insert(3);
   list2.insert(6);
   list2.insert(9);

   list2 = std::move(list1);

   EXPECT_EQ(list2[0], 2)
   EXPECT_EQ(list2[1], 4)
   EXPECT_EQ(list2[2], 6)
}END_TEST

BEGIN_TEST(DoubleList, SequentialSearchFound)
{
   dsa::SequentialSearch searchSequential;
   dsa::DoubleList<int> list;
   list.insert(1);
   list.insert(2);
   list.insert(3);
   list.insert(4);
   list.insert(5);

   int index = searchSequential(list, 3);
   EXPECT_EQ(list[index], 3)
}END_TEST

BEGIN_TEST(DoubleList, SequentialSearchNotFound)
{
   dsa::SequentialSearch searchSequential;
   dsa::DoubleList<int> list;
   list.insert(1);
   list.insert(2);
   list.insert(3);
   list.insert(4);
   list.insert(5);

   int index = searchSequential(list, 6);
   EXPECT_EQ(index, -1)
}END_TEST

BEGIN_TEST(DoubleList, BinarySearchFoundOdd)
{
   dsa::BinarySearch searchBinary;
   dsa::DoubleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(8);
   list.insert(12);

   int index = searchBinary(list, 2);
   EXPECT_EQ(list[index], 2)
}END_TEST

BEGIN_TEST(DoubleList, BinarySearchFoundEven)
{
   dsa::BinarySearch searchBinary;
   dsa::DoubleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(8);
   list.insert(10);
   list.insert(12);

   int index = searchBinary(list, 2);
   EXPECT_EQ(list[index], 2)
}END_TEST

BEGIN_TEST(DoubleList, BinarySearchNotFoundOdd)
{
   dsa::BinarySearch searchBinary;
   dsa::DoubleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(8);
   list.insert(12);

   int index = searchBinary(list, 7);
   EXPECT_EQ(index, -1)
}END_TEST

BEGIN_TEST(DoubleList, BinarySearchNotFoundEven)
{
   dsa::BinarySearch searchBinary;
   dsa::DoubleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(8);
   list.insert(10);
   list.insert(12);

   int index = searchBinary(list, 7);
   EXPECT_EQ(index, -1)
}END_TEST

BEGIN_TEST(DoubleList, FibonacciSearchFoundLHS)
{
   dsa::FibonacciSearch searchFibonacci;
   dsa::DoubleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(7);
   list.insert(8);
   list.insert(10);

   int index = searchFibonacci(list, 2);
   EXPECT_EQ(list[index], 2)
}END_TEST

BEGIN_TEST(DoubleList, FibonacciSearchFoundRHS)
{
   dsa::FibonacciSearch searchFibonacci;
   dsa::DoubleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(7);
   list.insert(8);
   list.insert(10);

   int index = searchFibonacci(list, 10);
   EXPECT_EQ(list[index], 10)
}END_TEST

BEGIN_TEST(DoubleList, FibonacciSearchNotFound)
{
   dsa::FibonacciSearch searchFibonacci;
   dsa::DoubleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(7);
   list.insert(8);
   list.insert(10);

   int index1 = searchFibonacci(list, 11);
   int index2 = searchFibonacci(list, 1);
   EXPECT_EQ(index1, -1)
   EXPECT_EQ(index2, -1)
}END_TEST

BEGIN_TEST(DoubleList, InsertionSort)
{
   dsa::InsertionSort inSort;
   dsa::DoubleList<int> list;
   list.insert(3);
   list.insert(1);
   list.insert(2);
   list.insert(5);
   list.insert(4);

   inSort(list);

   for(int i = 1; i <= list.size(); ++i)
   {
      EXPECT_EQ(list[i - 1], i)
   }
}END_TEST

BEGIN_TEST(DoubleList, QuickSort)
{
   dsa::QuickSort qSort;
   dsa::DoubleList<int> list;
   list.insert(3);
   list.insert(1);
   list.insert(2);
   list.insert(5);
   list.insert(4);

   qSort(list);

   for(int i = 1; i <= list.size(); ++i)
   {
      EXPECT_EQ(list[i - 1], i)
   }
}END_TEST

BEGIN_TEST(DoubleList, MergeSort)
{
   dsa::MergeSort mSort;
   dsa::DoubleList<int> list;
   list.insert(3);
   list.insert(1);
   list.insert(2);
   list.insert(5);
   list.insert(4);

   dsa::DoubleList<int> ret{mSort(list)};

   for(int i = 1; i <= list.size(); ++i)
   {
      EXPECT_EQ(ret[i - 1], i)
   }
}END_TEST

BEGIN_TEST(DoubleList, HeapSort)
{
   dsa::HeapSort hSort;
   dsa::DoubleList<int> list;
   list.insert(3);
   list.insert(1);
   list.insert(2);
   list.insert(5);
   list.insert(4);

   hSort(list);

   for(int i = 1; i <= list.size(); ++i)
   {
      EXPECT_EQ(list[i - 1], i)
   }
}END_TEST

BEGIN_TEST(DoubleList, RadixSort)
{
   dsa::RadixSort rSort;
   dsa::DoubleList<int> list;
   list.insert(3);
   list.insert(1);
   list.insert(2);
   list.insert(5);
   list.insert(4);

   rSort(list, 1);

   for(int i = 1; i <= list.size(); ++i)
   {
      EXPECT_EQ(list[i - 1], i)
   }
}END_TEST
