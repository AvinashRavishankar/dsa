#include "test_DoubleNode.hpp"

BEGIN_TEST(DoubleNode, Data)
{
   dsa::DoubleNode<int> node1;
   EXPECT_EQ(node1.data(), static_cast<int>(NULL))

   dsa::DoubleNode<int> node2(10);
   EXPECT_EQ(node2.data(), 10)
}END_TEST

BEGIN_TEST(DoubleNode, LeftNode)
{
   dsa::DoubleNode<int> node1;
   EXPECT_EQ(node1.leftNode(), nullptr)

   dsa::DoubleNode<int> node2(0, &node1);
   EXPECT_EQ(node2.leftNode(), &node1)
}END_TEST

BEGIN_TEST(DoubleNode, RightNode)
{
   dsa::DoubleNode<int> node1;
   EXPECT_EQ(node1.rightNode(), nullptr)

   dsa::DoubleNode<int> node2(0, nullptr, &node1);
   EXPECT_EQ(node2.rightNode(), &node1)
}END_TEST

BEGIN_TEST(DoubleNode, CopyAssignment)
{
   dsa::DoubleNode<int> node1(5);
   dsa::DoubleNode<int> node2(10, &node1);
   dsa::DoubleNode<int> node3(15, nullptr, &node1);

   node2 = node3;
   EXPECT_EQ(node2.data(), node3.data())
   EXPECT_EQ(node2.leftNode(), node3.leftNode())
   EXPECT_EQ(node2.rightNode(), node3.rightNode())
}END_TEST

BEGIN_TEST(DoubleNode, MoveAssignment)
{
   dsa::DoubleNode<int> node1(5);
   dsa::DoubleNode<int> node2(10, &node1);
   dsa::DoubleNode<int> node3(15, nullptr, &node1);

   node2 = std::move(node3);
   EXPECT_EQ(node2.data(), 15)
   EXPECT_EQ(node2.leftNode(), nullptr)
   EXPECT_EQ(node2.rightNode(), &node1)
   EXPECT_EQ(node3.data(), static_cast<int>(NULL))
   EXPECT_EQ(node3.leftNode(), nullptr)
   EXPECT_EQ(node3.rightNode(), nullptr)
}END_TEST
