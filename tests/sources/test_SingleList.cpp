#include "test_SingleList.hpp"

BEGIN_TEST(SingleList, Empty)
{
   dsa::SingleList<int> list;
   EXPECT_EQ(list.isEmpty(), true)
}END_TEST

BEGIN_TEST(SingleList, Insert)
{
   dsa::SingleList<int> list;
   list.insert(5);
   list.insert(10);
   list.insert(15);
   EXPECT_EQ(list[0], 5)
   EXPECT_EQ(list[1], 10)
   EXPECT_EQ(list[2], 15)
}END_TEST

BEGIN_TEST(SingleList, InsertAtPosition)
{
   dsa::SingleList<int> list;
   list.insertAt(0, 5);
   list.insertAt(1, 15);
   list.insertAt(1, 10);
   EXPECT_EQ(list[0], 5)
   EXPECT_EQ(list[1], 10)
   EXPECT_EQ(list[2], 15)
}END_TEST

BEGIN_TEST(SingleList, Remove)
{
   dsa::SingleList<int> list;
   list.insert(5);
   list.insert(10);
   list.insert(15);

   list.remove();
   try
   {
      list[2];
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(list[1], 10)
      EXPECT_EQ(list[0], 5)
   }

   list.remove();
   try
   {
      list[1];
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(list[0], 5)
   }

   list.remove();
   try
   {
      list[0];
      EXPECT_EQ(true, false)
   }
   catch(const std::logic_error& e)
   {
      EXPECT_EQ(list.isEmpty(), true)
   }
}END_TEST

BEGIN_TEST(SingleList, RemoveAtPosition)
{
   dsa::SingleList<int> list;
   list.insert(5);
   list.insert(10);
   list.insert(15);

   list.removeAt(0);
   try
   {
      list[2];
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(list[0], 10)
      EXPECT_EQ(list[1], 15)
   }

   list.removeAt(1);
   try
   {
      list[1];
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(list[0], 10)
   }
}END_TEST

BEGIN_TEST(SingleList, CopyAssignment)
{
   dsa::SingleList<int> list1;
   list1.insert(2);
   list1.insert(4);
   list1.insert(6);

   dsa::SingleList<int> list2;
   list2.insert(3);
   list2.insert(6);
   list2.insert(9);

   list2 = list1;

   EXPECT_EQ(list2[0], list1[0])
   EXPECT_EQ(list2[1], list1[1])
   EXPECT_EQ(list2[2], list1[2])
}END_TEST

BEGIN_TEST(SingleList, MoveAssignment)
{
   dsa::SingleList<int> list1;
   list1.insert(2);
   list1.insert(4);
   list1.insert(6);

   dsa::SingleList<int> list2;
   list2.insert(3);
   list2.insert(6);
   list2.insert(9);

   list2 = std::move(list1);

   EXPECT_EQ(list2[0], 2)
   EXPECT_EQ(list2[1], 4)
   EXPECT_EQ(list2[2], 6)
}END_TEST

BEGIN_TEST(SingleList, SequentialSearchFound)
{
   dsa::SequentialSearch searchSequential;
   dsa::SingleList<int> list;
   list.insert(1);
   list.insert(2);
   list.insert(3);
   list.insert(4);
   list.insert(5);

   int index = searchSequential(list, 3);
   EXPECT_EQ(list[index], 3)
}END_TEST

BEGIN_TEST(SingleList, SequentialSearchNotFound)
{
   dsa::SequentialSearch searchSequential;
   dsa::SingleList<int> list;
   list.insert(1);
   list.insert(2);
   list.insert(3);
   list.insert(4);
   list.insert(5);

   int index = searchSequential(list, 6);
   EXPECT_EQ(index, -1)
}END_TEST

BEGIN_TEST(SingleList, BinarySearchFoundOdd)
{
   dsa::BinarySearch searchBinary;
   dsa::SingleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(8);
   list.insert(12);

   int index = searchBinary(list, 2);
   EXPECT_EQ(list[index], 2)
}END_TEST

BEGIN_TEST(SingleList, BinarySearchFoundEven)
{
   dsa::BinarySearch searchBinary;
   dsa::SingleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(8);
   list.insert(10);
   list.insert(12);

   int index = searchBinary(list, 2);
   EXPECT_EQ(list[index], 2)
}END_TEST

BEGIN_TEST(SingleList, BinarySearchNotFoundOdd)
{
   dsa::BinarySearch searchBinary;
   dsa::SingleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(8);
   list.insert(12);

   int index = searchBinary(list, 7);
   EXPECT_EQ(index, -1)
}END_TEST

BEGIN_TEST(SingleList, BinarySearchNotFoundEven)
{
   dsa::BinarySearch searchBinary;
   dsa::SingleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(8);
   list.insert(10);
   list.insert(12);

   int index = searchBinary(list, 7);
   EXPECT_EQ(index, -1)
}END_TEST

BEGIN_TEST(SingleList, FibonacciSearchFoundLHS)
{
   dsa::FibonacciSearch searchFibonacci;
   dsa::SingleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(7);
   list.insert(8);
   list.insert(10);

   int index = searchFibonacci(list, 2);
   EXPECT_EQ(list[index], 2)
}END_TEST

BEGIN_TEST(SingleList, FibonacciSearchFoundRHS)
{
   dsa::FibonacciSearch searchFibonacci;
   dsa::SingleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(7);
   list.insert(8);
   list.insert(10);

   int index = searchFibonacci(list, 10);
   EXPECT_EQ(list[index], 10)
}END_TEST

BEGIN_TEST(SingleList, FibonacciSearchNotFound)
{
   dsa::FibonacciSearch searchFibonacci;
   dsa::SingleList<int> list;
   list.insert(2);
   list.insert(4);
   list.insert(6);
   list.insert(7);
   list.insert(8);
   list.insert(10);

   int index1 = searchFibonacci(list, 11);
   int index2 = searchFibonacci(list, 1);
   EXPECT_EQ(index1, -1)
   EXPECT_EQ(index2, -1)
}END_TEST

BEGIN_TEST(SingleList, MergeSort)
{
   dsa::MergeSort mSort;
   dsa::SingleList<int> list;
   list.insert(3);
   list.insert(1);
   list.insert(2);
   list.insert(5);
   list.insert(4);

   dsa::SingleList<int> ret{mSort(list)};

   for(int i{1}; i <= ret.size(); ++i)
   {
      EXPECT_EQ(ret[i - 1], i)
   }
}END_TEST

BEGIN_TEST(SingleList, HeapSort)
{
   dsa::HeapSort hSort;
   dsa::SingleList<int> list;
   list.insert(3);
   list.insert(1);
   list.insert(2);
   list.insert(5);
   list.insert(4);

   hSort(list);

   for(int i{1}; i <= list.size(); ++i)
   {
      EXPECT_EQ(list[i - 1], i)
   }
}END_TEST

BEGIN_TEST(SingleList, RadixSort)
{
   dsa::RadixSort rSort;
   dsa::SingleList<int> list;
   list.insert(3);
   list.insert(1);
   list.insert(2);
   list.insert(5);
   list.insert(4);

   rSort(list, 1);

   for(int i{1}; i <= list.size(); ++i)
   {
      EXPECT_EQ(list[i - 1], i)
   }
}END_TEST
