#include "test_BinarySearchTree.hpp"
#include "Array.hpp"

BEGIN_TEST(BinarySearchTree, Empty)
{
   dsa::BinarySearchTree<int> bTree;
   EXPECT_EQ(bTree.isEmpty(), true);
}END_TEST

BEGIN_TEST(BinarySearchTree, Insert)
{
   dsa::BinarySearchTree<int> bTree;
   bTree.insert(5);
   bTree.insert(3);
   bTree.insert(7);

   dsa::DoubleList<int> actual = bTree.traversePreOrder();
   dsa::DoubleList<int> expected;
   expected.insert(5);
   expected.insert(3);
   expected.insert(7);

   for(int i{0}; i < expected.size(); ++i)
   {
      EXPECT_EQ(actual[i], expected[i])
   }
}END_TEST

BEGIN_TEST(BinarySearchTree, GetMin)
{
   dsa::BinarySearchTree<int> bTree;
   bTree.insert(5);
   bTree.insert(3);
   bTree.insert(7);

   EXPECT_EQ(bTree.getMin(), 3)
}END_TEST

BEGIN_TEST(BinarySearchTree, GetMax)
{
   dsa::BinarySearchTree<int> bTree;
   bTree.insert(5);
   bTree.insert(3);
   bTree.insert(7);

   EXPECT_EQ(bTree.getMax(), 7)
}END_TEST

BEGIN_TEST(BinarySearchTree, RemoveNode)
{
   dsa::BinarySearchTree<int> bTree;
   bTree.insert(8);
   bTree.insert(9);
   bTree.insert(4);
   bTree.insert(1);
   bTree.insert(3);
   bTree.insert(5);
   bTree.insert(10);
   bTree.insert(7);
   bTree.insert(2);
   bTree.insert(6);

   bTree.remove(5);
   dsa::DoubleList<int> actual = bTree.traversePreOrder();
   dsa::DoubleList<int> expected;
   expected.insert(8);
   expected.insert(4);
   expected.insert(1);
   expected.insert(3);
   expected.insert(2);
   expected.insert(6);
   expected.insert(7);
   expected.insert(9);
   expected.insert(10);

   for(int i{0}; i < expected.size(); ++i)
   {
      EXPECT_EQ(actual[i], expected[i])
   }

   bTree.remove(2);
   actual = bTree.traversePreOrder();
   dsa::DoubleList<int> expected1;
   expected1.insert(8);
   expected1.insert(4);
   expected1.insert(1);
   expected1.insert(3);
   expected1.insert(6);
   expected1.insert(7);
   expected1.insert(9);
   expected1.insert(10);

   for(int i{0}; i < expected1.size(); ++i)
   {
      EXPECT_EQ(actual[i], expected1[i])
   }

   bTree.remove(4);
   actual = bTree.traversePreOrder();
   dsa::DoubleList<int> expected2;
   expected2.insert(8);
   expected2.insert(6);
   expected2.insert(1);
   expected2.insert(3);
   expected2.insert(7);
   expected2.insert(9);
   expected2.insert(10);

   for(int i{0}; i < expected2.size(); ++i)
   {
      EXPECT_EQ(actual[i], expected2[i])
   }
}END_TEST

BEGIN_TEST(BinarySearchTree, BalancedTree)
{
   dsa::Array<int, 5> testArray{4, 1, 2, 5, 3};
   dsa::BinarySearchTree<int> testBST(testArray, BALANCED);

   EXPECT_EQ(testBST.root(), 3)
}END_TEST

BEGIN_TEST(BinarySearchTree, ImbalancedTree)
{
   dsa::Array<int, 5> testArray{4, 1, 2, 5, 3};
   dsa::BinarySearchTree<int> testBST(testArray);

   EXPECT_EQ(testBST.root(), 2)
}END_TEST
