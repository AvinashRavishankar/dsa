#include "test_Queue.hpp"

BEGIN_TEST(Queue, EmptyQueue)
{
   dsa::Queue<int, 2> queue;
   EXPECT_EQ(queue.isEmpty(), true)
}END_TEST

BEGIN_TEST(Queue, FrontEmptyQueue)
{
   dsa::Queue<int, 2> queue;
   try
   {
      queue.front();
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(true, true)
   }
}END_TEST

BEGIN_TEST(Queue, FrontNonEmptyQueue)
{
   dsa::Queue<int, 2> queue;
   queue.enqueue(2);
   EXPECT_EQ(queue.front(), 2)
}END_TEST

BEGIN_TEST(Queue, Enqueue)
{
   dsa::Queue<int, 2> queue;
   queue.enqueue(2);
   EXPECT_EQ(queue.front(), 2)
   EXPECT_EQ(queue.isEmpty(), false)
}END_TEST

BEGIN_TEST(Queue, Dequeue)
{
   dsa::Queue<int, 3> queue;
   queue.enqueue(2);
   queue.enqueue(4);

   EXPECT_EQ(queue.front(), 2)
   queue.dequeue();
   EXPECT_EQ(queue.front(), 4)
}END_TEST

BEGIN_TEST(Queue, Overflow)
{
   dsa::Queue<int, 2> queue;
   queue.enqueue(2);
   try
   {
      queue.enqueue(4);
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(true, true)
   }
}END_TEST

BEGIN_TEST(Queue, DequeueEmptyQueue)
{
   dsa::Queue<int, 2> queue;
   try
   {
      queue.dequeue();
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(true, true)
   }
}END_TEST
