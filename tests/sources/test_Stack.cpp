#include "test_Stack.hpp"

BEGIN_TEST(Stack, EmptyStack)
{
   dsa::Stack<int, 2> stack;
   EXPECT_EQ(stack.isEmpty(), true)
}END_TEST

BEGIN_TEST(Stack, TopEmptyStack)
{
   dsa::Stack<int, 2> stack;
   try
   {
      stack.top();
      EXPECT_EQ(true, false)
   }
   catch(const std::logic_error& e)
   {
      EXPECT_EQ(true, true)
   }
}END_TEST

BEGIN_TEST(Stack, TopNonEmptyStack)
{
   dsa::Stack<int, 2> stack;
   stack.push(2);
   try
   {
      EXPECT_EQ(stack.top(), 2)
   }
   catch(const std::logic_error& e)
   {
      EXPECT_EQ(true, false)
   }
}END_TEST

BEGIN_TEST(Stack, Push)
{
   dsa::Stack<int, 2> stack;
   stack.push(2);
   EXPECT_EQ(stack.top(), 2)
}END_TEST

BEGIN_TEST(Stack, Pop)
{
   dsa::Stack<int, 2> stack;
   stack.push(2);
   stack.push(4);

   EXPECT_EQ(stack.top(), 4)
   stack.pop();
   EXPECT_EQ(stack.top(), 2)
}END_TEST

BEGIN_TEST(Stack, Overflow)
{
   dsa::Stack<int, 2> stack;
   stack.push(2);
   stack.push(4);
   try
   {
      stack.push(6);
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(true, true)
   }
}END_TEST

BEGIN_TEST(Stack, PopEmptyStack)
{
   dsa::Stack<int, 2> stack;
   try
   {
      stack.pop();
      EXPECT_EQ(true, false)
   }
   catch(const std::out_of_range& e)
   {
      EXPECT_EQ(true, true)
   }
}END_TEST
