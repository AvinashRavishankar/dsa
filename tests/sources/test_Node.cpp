#include "test_Node.hpp"
#include <utility>

BEGIN_TEST(Node, Data)
{
   dsa::Node<int> node1;
   EXPECT_EQ(node1.data(), static_cast<int>(NULL))

   dsa::Node<int> node2(10);
   EXPECT_EQ(node2.data(), 10)
}END_TEST

BEGIN_TEST(Node, Node)
{
   dsa::Node<int> node1;
   EXPECT_EQ(node1.node(), nullptr)

   dsa::Node<int> node2(5, &node1);
   EXPECT_EQ(node2.node(), &node1)
}END_TEST

BEGIN_TEST(Node, CopyAssignment)
{
   dsa::Node<int> node;
   dsa::Node<int> node1(5);
   dsa::Node<int> node2(10, &node);

   node1 = node2;
   EXPECT_EQ(node1.data(), node2.data())
   EXPECT_EQ(node1.node(), node2.node())
}END_TEST

BEGIN_TEST(Node, MoveAssignment)
{
   dsa::Node<int> node;
   dsa::Node<int> node1(5);
   dsa::Node<int> node2(10, &node);

   node1 = std::move(node2);
   EXPECT_EQ(node1.data(), 10)
   EXPECT_EQ(node1.node(), &node)
   EXPECT_EQ(node2.data(), static_cast<int>(NULL))
   EXPECT_EQ(node2.node(), nullptr)
}END_TEST
