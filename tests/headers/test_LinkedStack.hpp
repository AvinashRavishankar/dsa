#ifndef TEST_LINKEDSTACK_INCLUDED
#define TEST_LINKEDSTACK_INCLUDED

#include "unittest.hpp"
#include "LinkedStack.hpp"

TEST(LinkedStack, EmptyStack)
TEST(LinkedStack, TopEmptyStack)
TEST(LinkedStack, TopNonEmptyStack)
TEST(LinkedStack, Push)
TEST(LinkedStack, Pop)
TEST(LinkedStack, PopEmptyStack)
TEST(LinkedStack, CopyAssignment)
TEST(LinkedStack, MoveAssignment)

#endif
