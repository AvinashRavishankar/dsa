#ifndef TEST_SINGLELIST_INCLUDED
#define TEST_SINGLELIST_INCLUDED

#include "unittest.hpp"
#include "HeapSort.hpp"
#include "MergeSort.hpp"
#include "SingleList.hpp"
#include "RadixSort.hpp"
#include "SearchAlgorithms.hpp"

TEST(SingleList, Empty)
TEST(SingleList, Insert)
TEST(SingleList, InsertAtPosition)
TEST(SingleList, Remove)
TEST(SingleList, RemoveAtPosition)
TEST(SingleList, CopyAssignment)
TEST(SingleList, MoveAssignment)
TEST(SingleList, SequentialSearchFound)
TEST(SingleList, SequentialSearchNotFound)
TEST(SingleList, BinarySearchFoundOdd)
TEST(SingleList, BinarySearchFoundEven)
TEST(SingleList, BinarySearchNotFoundOdd)
TEST(SingleList, BinarySearchNotFoundEven)
TEST(SingleList, FibonacciSearchFoundLHS)
TEST(SingleList, FibonacciSearchFoundRHS)
TEST(SingleList, FibonacciSearchNotFound)
TEST(SingleList, MergeSort)
TEST(SingleList, HeapSort)
TEST(SingleList, RadixSort)

#endif
