#ifndef TEST_BINARYSEARCHTREE_INCLUDED
#define TEST_BINARYSEARCHTREE_INCLUDED

#include "unittest.hpp"
#include "BinarySearchTree.hpp"

TEST(BinarySearchTree, Empty)
TEST(BinarySearchTree, Insert)
TEST(BinarySearchTree, GetMin)
TEST(BinarySearchTree, GetMax)
TEST(BinarySearchTree, RemoveNode)
TEST(BinarySearchTree, BalancedTree)
TEST(BinarySearchTree, ImbalancedTree)

#endif
