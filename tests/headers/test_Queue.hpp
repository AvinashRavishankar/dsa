#ifndef TEST_QUEUE_INCLUDED
#define TEST_QUEUE_INCLUDED

#include "unittest.hpp"
#include "Queue.hpp"

TEST(Queue, EmptyQueue)
TEST(Queue, FrontEmptyQueue)
TEST(Queue, FrontNonEmptyQueue)
TEST(Queue, Enqueue)
TEST(Queue, Dequeue)
TEST(Queue, Overflow)
TEST(Queue, DequeueEmptyQueue)

#endif
