#ifndef TEST_STACK_INCLUDED
#define TEST_STACK_INCLUDED

#include "unittest.hpp"
#include "Stack.hpp"

TEST(Stack, EmptyStack)
TEST(Stack, TopEmptyStack)
TEST(Stack, TopNonEmptyStack)
TEST(Stack, Push)
TEST(Stack, Pop)
TEST(Stack, Overflow)
TEST(Stack, PopEmptyStack)

#endif
