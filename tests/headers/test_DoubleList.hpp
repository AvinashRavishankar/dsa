#ifndef TEST_DOUBLELIST_INCLUDED
#define TEST_DOUBLELIST_INCLUDED

#include "unittest.hpp"
#include "HeapSort.hpp"
#include "MergeSort.hpp"
#include "QuickSort.hpp"
#include "RadixSort.hpp"
#include "DoubleList.hpp"
#include "InsertionSort.hpp"
#include "SearchAlgorithms.hpp"

TEST(DoubleList, Empty)
TEST(DoubleList, Insert)
TEST(DoubleList, InsertAtPosition)
TEST(DoubleList, Remove)
TEST(DoubleList, RemoveAtPosition)
TEST(DoubleList, CopyAssignment)
TEST(DoubleList, MoveAssignment)
TEST(DoubleList, SequentialSearchFound)
TEST(DoubleList, SequentialSearchNotFound)
TEST(DoubleList, BinarySearchFoundOdd)
TEST(DoubleList, BinarySearchFoundEven)
TEST(DoubleList, BinarySearchNotFoundOdd)
TEST(DoubleList, BinarySearchNotFoundEven)
TEST(DoubleList, FibonacciSearchFoundLHS)
TEST(DoubleList, FibonacciSearchFoundRHS)
TEST(DoubleList, FibonacciSearchNotFound)
TEST(DoubleList, InsertionSort)
TEST(DoubleList, QuickSort)
TEST(DoubleList, MergeSort)
TEST(DoubleList, HeapSort)
TEST(DoubleList, RadixSort)

#endif
