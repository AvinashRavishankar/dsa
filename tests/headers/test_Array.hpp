#ifndef TEST_ARRAY_INCLUDED
#define TEST_ARRAY_INCLUDED

#include "Array.hpp"
#include "unittest.hpp"
#include "HeapSort.hpp"
#include "QuickSort.hpp"
#include "MergeSort.hpp"
#include "RadixSort.hpp"
#include "InsertionSort.hpp"
#include "SearchAlgorithms.hpp"

TEST(Array, CopyAssignment)
TEST(Array, MoveAssignment)
TEST(Array, SequentialSearchFound)
TEST(Array, SequentialSearchNotFound)
TEST(Array, BinarySearchFoundOdd)
TEST(Array, BinarySearchFoundEven)
TEST(Array, BinarySearchNotFoundOdd)
TEST(Array, BinarySearchNotFoundEven)
TEST(Array, FibonacciSearchFoundLHS)
TEST(Array, FibonacciSearchFoundRHS)
TEST(Array, FibonacciSearchNotFound)
TEST(Array, InsertionSort)
TEST(Array, QuickSort)
TEST(Array, MergeSort)
TEST(Array, HeapSort)
TEST(Array, RadixSort_1)
TEST(Array, RadixSort_2)

#endif
