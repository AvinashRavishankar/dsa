#ifndef TEST_BINARYTREE_INCLUDED
#define TEST_BINARYTREE_INCLUDED

#include "unittest.hpp"
#include "BinaryTree.hpp"

TEST(BinaryTree, Empty)
TEST(BinaryTree, Insert)
TEST(BinaryTree, TraversePreOrder)
TEST(BinaryTree, TraverseInOrder)
TEST(BinaryTree, TraversePostOrder)
TEST(BinaryTree, AreEqual)
TEST(BinaryTree, AreNotEqual)

#endif
