#ifndef TEST_LINKEDQUEUE_INCLUDED
#define TEST_LINKEDQUEUE_INCLUDED

#include "unittest.hpp"
#include "LinkedQueue.hpp"

TEST(LinkedQueue, EmptyQueue)
TEST(LinkedQueue, FrontEmptyQueue)
TEST(LinkedQueue, FrontNonEmptyQueue)
TEST(LinkedQueue, Enqueue)
TEST(LinkedQueue, Dequeue)
TEST(LinkedQueue, DequeueEmptyQueue)
TEST(LinkedQueue, CopyAssignment)
TEST(LinkedQueue, MoveAssignment)

#endif
