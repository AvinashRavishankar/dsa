# Data Structures and Algorithms

***A lightweight, header-only C++ template library for common data
structures and algorithms***

This project started with the intention of learning common *data structures* and
*algorithms* through implementation, as an alternative to STL. At present, it
depends on the C++ Standard Library (libstdc++) for exception handling, memory
and IO. The final goal is to get rid of these dependencies, thereby, allowing
custom exception handling, memory and IO implementations on bare-metal systems.

Visit the code repository [<b>here</b>](https://gitlab.com/AvinashRavishankar/dsa).

## Installation Guide

1. Simply clone the repository into your project and include the necessary
   header files. No installation needed.
```sh
git clone https://gitlab.com/AvinashRavishankar/dsa.git
```

2. If you want to install it onto your system, run the following commands.
   ***Note:*** requires `cmake`.
```sh
git clone https://gitlab.com/AvinashRavishankar/dsa.git
mkdir -p dsa/build
cd dsa/build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
make install
```
The default installation path is `/usr/local/include/dsa`. If you wish to
install it elsewhere, use
```sh
make install prefix=<directory>
```
To build tests for this library, append `-DBUILD_TESTS=ON` to the `cmake`
command above.

## Usage

The library provides four basic data structures: arrays, singly-linked
lists, doubly-linked lists, and trees.

Here's how you can use the Array class in your Project:

```cpp
// Create an array
#include "Array.hpp"

dsa::Array<int, 5> array1;                   // Initializes array1 to 0's
array1.set(1, 3);
std::cout << array1[1] << std::endl;         // Prints 3

dsa::Array<int, 5> array2{1, 2, 3, 4, 5};
for(auto x : array2)
   std::cout << x << std::endl;              // Prints 1, 2, 3, 4, 5
```
By default, arrays are 0-initialized at compile time when no values are
provided. The `set` function can be used to change the value at a particular
index.

Both arrays and lists have built-in __iterator__ classes that allow using
range-based loops for added convenience.

Search and Sort algorithms are implemented as _function objects_. The below
code snippet demonstrates the usage of the binary search and quick sort
algorithms.
```cpp
#include "DoubleList.hpp"

dsa::DoubleList<int> aList;
aList.insert(3);
aList.insert(1);
aList.insert(2);
aList.insert(5);
aList.insert(4);

dsa::BinSearch searchBin;
int indexT{searchBin(aList, 2)};
int indexF{searchBin(aList, 7)};

std::cout << indexT << std::endl;   // Prints 2: Found 2 at 2
std::cout << indexF << std::endl;   // Prints -1: Number not found

dsa::QSort qSort;
qSort(aList);
indexT = searchBin(aList, 2);
std::cout << indexT << std::endl;   // Prints 1: Found 2 at 1
```

The library provides two variants of _Stacks_ and _Queues_: Array-based, and
List-based.

Array-based Stack and Queue data structures require a definite size as a
template argument. Whereas, the list-based data structures can take any
positive number of elements until your program runs out of heap memory.
```cpp
dsa::LinkedStack<int> lStack;
dsa::LinkedQueue<int> lQueue;

dsa::Stack<int, 10> stack;
dsa::Queue<int, 10> queue;
```

## License

> Copyright 2021 Avinash Chikkadlur Ravi Shankar
>
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
>
>   http://www.apache.org/licenses/LICENSE-2.0
>
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.

